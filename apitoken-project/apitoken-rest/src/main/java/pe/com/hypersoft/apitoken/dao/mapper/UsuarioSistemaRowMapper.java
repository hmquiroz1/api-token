package pe.com.hypersoft.apitoken.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import pe.com.hypersoft.apitoken.bean.UsuarioRestBean;
import pe.com.hypersoft.apitoken.bean.UsuarioSistemaBean;

@Component
@SuppressWarnings("rawtypes")
public class UsuarioSistemaRowMapper implements RowMapper {

	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {

		UsuarioSistemaBean usuario = new UsuarioSistemaBean();
		usuario.setId(rs.getLong("ID_USUARIO"));
		usuario.setEmail(rs.getString("EMAIL"));
		usuario.setEstado(rs.getString("ESTADO"));
		
		return usuario;
	}
}