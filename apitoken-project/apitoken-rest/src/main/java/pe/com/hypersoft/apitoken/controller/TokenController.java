package pe.com.hypersoft.apitoken.controller;

import java.io.IOException;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;

import pe.com.hypersoft.apitoken.bean.ObjetoRespuesta;
import pe.com.hypersoft.apitoken.bean.UsuarioRestBean;
import pe.com.hypersoft.apitoken.bean.UsuarioSistemaBean;
import pe.com.hypersoft.apitoken.service.SeguridadService;
import pe.com.hypersoft.apitoken.util.Constante;

@Controller
@RequestMapping(value = "/api-token")
public class TokenController {
	
	@Autowired
	SeguridadService seguridadService;
		
	@RequestMapping(value = "/rest", method = RequestMethod.POST, headers = "Accept=application/json", produces = "text/plain;charset=UTF-8")
	public @ResponseBody ResponseEntity<Object> registrarSolicitud(@RequestBody Map<String, Object> map) {

		Constante.loggerTokenApi.debug("=== /api-token/rest : inicio ====");
		ObjetoRespuesta data = new ObjetoRespuesta();
		ObjectMapper mapper = new ObjectMapper();
		String json = StringUtils.EMPTY;
		try {
			json = mapper.writeValueAsString(map);
		} catch (IOException e) {
			Constante.loggerTokenApi.error(Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR+"al convertir el json de entrada.");
		}catch(Exception e){
			Constante.loggerTokenApi.error(Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR+"al convertir el json de entrada.");
		}

		Constante.loggerTokenApi.info("json : " + json);

		int resp = verificarDatoUsuario(mapper, map);

		if (resp == 1) {

			UsuarioSistemaBean usuarioData = null;
			//TarjetaFPFBean tarjetaFPF = null;
			int idRpta = 0;
			
			if (map.get("tipoServicio") != null) {
				String tipo = (String) map.get("tipoServicio");
				
				//Registro de la solicitud de tarjeta federacion
				/*if (tipo.equalsIgnoreCase(Constante.TIPO_OPERACION.OPERACION_REGISTRAR_TOKEN)) {
					Constante.loggerTokenApi.info("registrarSolicitud-entrada: " + json);
					tarjetaFPF = mapper.convertValue(map.get("objeto"), TarjetaFPFBean.class);
					
					idRpta = tarjetaDisenioService.registrarTarjetaDisenio(tarjetaFPF);
				}*/
				
				//Consulta para saber si una solicitud ya est� registrada.
				if (tipo.equalsIgnoreCase(Constante.TIPO_OPERACION.CONSULTAR_SI_EXISTE_TOKEN)) {					
					Constante.loggerTokenApi.info("consultarSiExisteToken-entrada: " + json);
					usuarioData = mapper.convertValue(map.get("objeto"), UsuarioSistemaBean.class);
					data = seguridadService.consultarSiExisteToken(usuarioData);
					data.setToken(usuarioData.getToken());
					idRpta = Integer.parseInt(data.getCodigo());
				}
				
				//Verificacion de rptas
				if (idRpta > 0) {
					data.setCodigo(String.valueOf(idRpta));
					data.setValor(Constante.MENSAJE.MSJ_OK);
				} else if (idRpta == -1) {
					data.setCodigo(Constante.PARAMETRO.COD_RPTA_EXCEPTION);
					data.setValor(Constante.MENSAJE.MSJ_ERROR);
				} else if (idRpta == 0) {
					data.setCodigo(Constante.PARAMETRO.COD_RPTA_NO_EXITO);
					data.setValor(Constante.MENSAJE.MSJ_NO_DATA);
				}
				
				Constante.loggerTokenApi.info("-----------------------------------");
				return new ResponseEntity<Object>(data, HttpStatus.CREATED);
			}

		} else {
			data.setCodigo(Constante.PARAMETRO.COD_RPTA_NO_EXITO);
			data.setValor(Constante.MENSAJE.MSJ_NO_DATA);
			Constante.loggerTokenApi.error("----- salida-ERROR: "+Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR + "al verificarDatoUsuario. ");
			return new ResponseEntity<Object>(data, HttpStatus.CREATED);
		}
		
		Constante.loggerTokenApi.debug("=== /api-token/rest : fin ====");

		return new ResponseEntity<Object>(HttpStatus.BAD_REQUEST);
	}
	
	//Validacion de usuario generico para consultar servicios gifole-ws
	private int verificarDatoUsuario(ObjectMapper mapper, Map<String, Object> map){		
		UsuarioRestBean usuarioWsBean = mapper.convertValue(map.get("security"), UsuarioRestBean.class);
		byte[] decodedUsuario = Base64.decodeBase64(usuarioWsBean.getUsuario());
		String usuario = new String(decodedUsuario);
		byte[] decodedPassword = Base64.decodeBase64(usuarioWsBean.getPassword());
		String password = new String(decodedPassword);
		
		int resp = seguridadService.autenticarUsuario(usuario, password);
		return resp;
	}

}
