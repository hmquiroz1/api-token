package pe.com.hypersoft.apitoken.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import pe.com.hypersoft.apitoken.bean.UsuarioRestBean;
import pe.com.hypersoft.apitoken.dao.SeguridadDao;
import pe.com.hypersoft.apitoken.dao.mapper.UsuarioRestRowMapper;
import pe.com.hypersoft.apitoken.dao.mapper.UsuarioSistemaRowMapper;

import java.util.List;
import pe.com.hypersoft.apitoken.bean.UsuarioSistemaBean;

@Repository
public class SeguridadDaoImpl implements SeguridadDao {

	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@Autowired
	UsuarioSistemaRowMapper usuarioSistemaRowMapper;
	
	@SuppressWarnings("unchecked")
	public int autenticarUsuario(String usuario, String password) {
		Integer id = 0;
		String sql = "SELECT U.ID_USUARIO AS ID FROM APP_SEGURIDAD.USUARIO U WHERE TRIM(U.REGISTRO) = '" + usuario + "' AND TRIM(U.CLAVE) = '" + password + "' AND U.ESTADO='A'";
		UsuarioRestBean param = (UsuarioRestBean) jdbcTemplate.queryForObject(
		sql, new Object[] {}, new UsuarioRestRowMapper());
		if(param!=null){
			if(param.getId().intValue()>0)
				id=1;
		}
		return id;
	}
	
	@SuppressWarnings("unchecked")
	public List<UsuarioSistemaBean> consultarSiExisteToken(UsuarioSistemaBean usuarioSistemaBean) {
		StringBuilder sql=new StringBuilder();
		sql.append("SELECT ID_USUARIO,EMAIL,ESTADO  FROM APP_SEGURIDAD.USUARIO  ");
		sql.append(" WHERE API_TOKEN = '" + usuarioSistemaBean.getToken() + "' ");
		return jdbcTemplate.query(sql.toString(), usuarioSistemaRowMapper);
	}
}
