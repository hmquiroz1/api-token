package pe.com.hypersoft.apitoken.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public abstract class Constante {

	public static Logger loggerSeguridad = LogManager.getLogger("seguridad");	
	public static Logger loggerGeneral = LogManager.getLogger("general");	
	public static Logger loggerTokenApi = LogManager.getLogger("tokenapi");
	public static Logger loggerUsuarioApi = LogManager.getLogger("usuarioapi");
	
	/**
	 * Respuestas de ejecucion<br>
	 * 1 Indica que la respuesta ha sido <b>�xito = 1</b><br>
	 * 0 Indica que la respuesta ha sido <font color="red"><b>error = 0</b></font><br>
	 * -1 Indica que ha ocurrido una <font color="red"> <b>excepcion = -1</b></font>
	 */
	public abstract class PARAMETRO {
		
		public static final String COD_RPTA_EXITO = "1";
		public static final String COD_RPTA_NO_EXITO = "0";
		public static final String COD_RPTA_EXCEPTION = "-1";
	}
	
	public abstract class MENSAJE {
		
		public static final String MSJ_RPTA_EXITO_CONSULTAR_APERTURA_CUENTA= "Se encontro el numero de cuenta consultada.";
		public static final String MSJ_RPTA_NO_EXITO_CONSULTAR_APERTURA_CUENTA= "No se encontro el numero de cuenta consultada."; 
		public static final String MSJ_RPTA_EXCEPTION = "Ha ocurrido un error en: ";
		public static final String MSJ_RPTA_NULL = "Error!. No exise el objeto.";
		public static final String MSJ_RPTA_VACIO = "Error!. Datos ingresados vacios.";
		public static final String MSJ_RPTA_LISTA_VACIA = "La lista obtenida se encuentra vacia.";
		
		public static final String MSJ_OK = "OK";
		public static final String MSJ_ERROR = "ERROR";
		public static final String MSJ_NO_DATA = "NO SE ENCONTRARON DATOS";
		public static final String MSJ_NO_PROCESO = "YA EXISTE EL ARCHIVO";
		
		public static final String MSJ_ERROR_NO_ACTUALIZA = "ERROR - NO ACTUALIZA";
		public static final String MSJ_ERROR_NO_COPIA_ARCHIVOS = "ERROR - NO COPIA ARCHIVOS";
		
		public static final String MSJ_HA_OCURRIDO_ERROR = "Ha ocurrido un error ";
		public static final String MSJ_TERMINO_CON_EXITO = "Se he culminado con exito ";
	}	
		
	public abstract class MONEDA {
		public static final String SOLES="SOLES";
		public static final String DOLARES="DOLARES";
	}	
	
	public abstract class TIPO_OPERACION{
		public static final String OPERACION_CONSULTAR_TOKEN = "CONS_TOKEN";
		public static final String OPERACION_REGISTRAR_TOKEN = "REG_TOKEN";
		public static final String CONSULTAR_SI_EXISTE_TOKEN = "CONSULTA_EXIST_TOKEN";
	}
}
