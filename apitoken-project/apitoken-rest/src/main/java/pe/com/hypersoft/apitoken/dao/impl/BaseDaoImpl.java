package pe.com.hypersoft.apitoken.dao.impl;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

public class BaseDaoImpl {
	
	@Autowired
	private DataSource dataSource;
		
	private JdbcTemplate jdbcTemplate;
	
	
	public void setDataSource(DataSource dataSource) {	 
        this.dataSource = dataSource;
        this.jdbcTemplate = new JdbcTemplate(this.dataSource);
    }
	
}
