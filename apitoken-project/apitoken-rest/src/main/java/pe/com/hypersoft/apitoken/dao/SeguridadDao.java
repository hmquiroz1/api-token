package pe.com.hypersoft.apitoken.dao;

import java.util.List;
import pe.com.hypersoft.apitoken.bean.UsuarioSistemaBean;

public interface SeguridadDao {

	int autenticarUsuario(String usuario,String password);
	List<UsuarioSistemaBean> consultarSiExisteToken(UsuarioSistemaBean usuarioSistemaBean);
}
