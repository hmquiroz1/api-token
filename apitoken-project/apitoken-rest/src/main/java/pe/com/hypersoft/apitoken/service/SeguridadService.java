package pe.com.hypersoft.apitoken.service;

import pe.com.hypersoft.apitoken.bean.ObjetoRespuesta;
import pe.com.hypersoft.apitoken.bean.UsuarioSistemaBean;

public interface SeguridadService {
	
	int autenticarUsuario(String usuario,String password);
	ObjetoRespuesta consultarSiExisteToken(UsuarioSistemaBean usuarioSistemaBean);
}
