package pe.com.hypersoft.apitoken.util;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

public class Funcion {
	
	public static String simboloMoneda(String moneda){
		String simbolo="";
		if(moneda.toUpperCase().indexOf("SOLES")>-1 || moneda.toUpperCase().indexOf("PEN")>-1){
			simbolo="S/";
		}else if(moneda.toUpperCase().indexOf("DOLARES")>-1 || moneda.toUpperCase().indexOf("USD")>-1){
			simbolo="$";
		}
		return simbolo;
	}
	
	public static Date stringToDate(String string, String format){
		SimpleDateFormat df = new SimpleDateFormat(format);
		try{
			return df.parse(string);
		} catch (Exception e) {
			Constante.loggerGeneral.error("Ha ocurrido un error en stringToDate: "+ e);
		}
		return null;
	}
	public static String saberDiaMesOAnhio(String valor){
		Calendar cal = Calendar.getInstance();
		String rpta = "";
		if(valor.equalsIgnoreCase("d")){
	    	int diaHoy = cal.get(Calendar.DAY_OF_MONTH);
	    	rpta=String.valueOf(diaHoy);
	    }if(valor.equalsIgnoreCase("m")){
	    	String[] mesesString = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		    int mesInt = cal.get(Calendar.MONTH)+1;
		    rpta = mesesString[mesInt-1];
	    }if(valor.equalsIgnoreCase("a")){
	    	int anhioHoy = cal.get(Calendar.YEAR);
	    	rpta=String.valueOf(anhioHoy);
	    }
		return rpta;
	}
	
	/**
	 * Formatear el importe tipo string
	 * 
	 *
	 * @param monto
	 *            Importe a formatear
	 * @param tipo
	 *            <code>1</code> con punto decimal con separador de miles / 
	 *            <code>2</code> sin decimales con separador de miles /
	 *            <code>3</code> sin decimales ni separador de miles
	 * @return fmonto
	 * 			Monto formateado
	 */
	public static String FormatoMonto(String monto, int tipo){
		Number numero;
		String numeroformateado="";
		try{
			java.text.DecimalFormatSymbols dfs = new java.text.DecimalFormatSymbols();
			dfs.setDecimalSeparator('.');
			dfs.setGroupingSeparator(',');
			java.text.DecimalFormat df;
			if (tipo==1){
				df = new java.text.DecimalFormat("#,###,###.00",dfs);}
			else if (tipo==2){
				df = new java.text.DecimalFormat("#,###,###",dfs);
			} else {
				monto=monto.replace(",","");
				df = new java.text.DecimalFormat("#######",dfs);
			}
			numero = df.parse(monto);
			numeroformateado= df.format(numero.doubleValue());
		}catch(Exception e){
			numeroformateado="0.00";
		}
		return numeroformateado;
	}
	
	public static String NullOrEmpty(Object obj) {

		if (obj == null) {

			return "";

		} else {

			return obj.toString();

		}

	}
	
	public static String revisar(String valor) {
		
		valor = StringUtils.stripAccents(valor);
		
		return valor;
	}
	
	public static String convertirEncoding(String cad){
		try {
			
			if(cad != null) {
				if(cad.toUpperCase().equals("NULL")){
					cad = "";
				}
				cad = new String(cad.getBytes("ISO-8859-15"), "UTF-8");
				return cad;
			}			
			
		} catch (UnsupportedEncodingException e) {
			Constante.loggerGeneral.error("Ha ocurrido un UnsupportedEncodingException al convertirEncoding:" + e);
		} catch (Exception e) {
			Constante.loggerGeneral.error("Ha ocurrido un error al convertirEncoding:" + e);
		}
		return cad;
	}
	
	public static int ejecutarSCP(String desde, String hacia) {
		StringBuilder strComando = new StringBuilder();
		strComando.append("/usr/bin/scp ");
		strComando.append(desde).append(" ");
		strComando.append(hacia);
		
		Constante.loggerGeneral.info("Comando a ejecutar:" + strComando.toString());
		
		Process pros;
		try {
			pros = Runtime.getRuntime().exec(strComando.toString());
			pros.waitFor();
			int valorSalida = pros.exitValue();
			if(valorSalida != 0) {
				Constante.loggerGeneral.error("Error scp: " + IOUtils.toString(pros.getErrorStream()));
				valorSalida=-1;
			}
			return valorSalida;
		} catch (Throwable e) {
			Constante.loggerGeneral.error("Error al ejecutar el comando scp", e);
		}
		
		return -1;
	}
	
	/**
	 * Completar caracteres a una cadena
	 * @param valor Cadena inicial a completar
	 * @param longitudCampo Se refiere a la longitud del campo a considerar
	 * @param tipoAlineacion Puede ser I: Izquierda / D: Derecha
	 * @param caracterRelleno El caracter a considerar como relleno.
	 * @return campoRelleno Es el valor obtenido despues de rellenar.
	 */
	public static String completarValor(String valor, String longitudCampo, String tipoAlineacion, String caracterRelleno){
		int longitud = Integer.parseInt(longitudCampo);
		String campoRelleno = StringUtils.trimToEmpty(valor);
		for(int i = valor.length(); i < longitud; i++){
			if(tipoAlineacion.equalsIgnoreCase("I")){
				campoRelleno+=caracterRelleno;
			}else{
				campoRelleno=caracterRelleno+campoRelleno;
			}
		}
		return campoRelleno;
	}
	
}
