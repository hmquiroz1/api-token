package pe.com.hypersoft.apitoken.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import pe.com.hypersoft.apitoken.bean.ObjetoRespuesta;
import pe.com.hypersoft.apitoken.bean.UsuarioSistemaBean;
import pe.com.hypersoft.apitoken.dao.SeguridadDao;
import pe.com.hypersoft.apitoken.service.SeguridadService;
import pe.com.hypersoft.apitoken.util.Constante;

@Service
public class SeguridadServiceImpl implements SeguridadService {
	
	@Autowired
	SeguridadDao seguridadDao;
	
	public int autenticarUsuario(String usuario, String password) {
		int resp= 0;
		
		try{
			resp = seguridadDao.autenticarUsuario(usuario, password);
		} catch(Exception e) {
			resp=-1;
			Constante.loggerSeguridad.error(Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR+"al autenticarUsuario ["+usuario+"] : " + e);
		}
		return resp;
	}
	
	public ObjetoRespuesta consultarSiExisteToken(UsuarioSistemaBean usuarioSistemaBean) {
		Constante.loggerTokenApi.info("========== INICIO consultarSiExisteToken ==========");		
		ObjetoRespuesta data = new ObjetoRespuesta();		
		List<UsuarioSistemaBean> listaUsuarios = new ArrayList<UsuarioSistemaBean>();
		
		try {			
			listaUsuarios = seguridadDao.consultarSiExisteToken(usuarioSistemaBean);			
			if(listaUsuarios.size() > 0){				
				Constante.loggerGeneral.info("Hay: "+listaUsuarios.size() + " registros encontrados.");
				data.setListaUsuarios(listaUsuarios);
				data.setCodigo(Constante.PARAMETRO.COD_RPTA_EXITO);
			} else {
				data.setCodigo(Constante.PARAMETRO.COD_RPTA_NO_EXITO);
			}
		} catch (Exception e) {
			data.setCodigo(Constante.PARAMETRO.COD_RPTA_EXCEPTION);
			Constante.loggerTokenApi.error(Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR+"al consultar si existe solicitud registrada: " + e);
		}
		Constante.loggerTokenApi.info("========== FIN consultarSiExisteToken ==========");
		return data;
	}
}
