package cifrado;


public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		 String emailDescifrado=null;
		 String email="hmquiroz1@hotmail.com";
		 String emailCifrado="";
		 TripleDesUtil objEncripta=null;
		 SHA1Util objSha=null;
		 String llaveAES="MZygpewJsCpRrfOr";
		 try {
			objEncripta=new TripleDesUtil();
			emailCifrado = objEncripta.encripta(email);
			System.out.println("Algoritmo TripleDes\n");
			System.out.println("emailCifrado="+emailCifrado);
			emailDescifrado= objEncripta.desencripta(emailCifrado);
			System.out.println("emailDescifrado="+emailDescifrado);
			
			/*******************************************************/
			objSha = new SHA1Util();
			System.out.println("\n Hash SHA1 \n");
			System.out.println("emailDescifrado="+objSha.hash(email));
			
			/*******************************************************/
			System.out.println("\n Algoritmo AES \n");
			byte[] llave = llaveAES.getBytes("UTF-8");
			AESUtil cu=new AESUtil(llave);
			String emailCifradoAES=cu.cifrarCadena(email);
			System.out.println("emailCifrado="+emailCifradoAES);
			String emailDescifradoAES=cu.decrypt(emailCifradoAES);
			System.out.println("emailDescifrado="+emailDescifradoAES);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
	}

}
