package cifrado;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

public class AESUtil {

    private byte[] key;

    private static final String ALGORITHM = "AES";

    public AESUtil(byte[] key)
    {
        this.key = key;
    }

    /**
     * Encrypts the given plain text
     *
     * @param plainText The plain text to encrypt
     */
    public byte[] encrypt(byte[] plainText) throws Exception
    {
        SecretKeySpec secretKey = new SecretKeySpec(key, ALGORITHM);
        Cipher cipher = Cipher.getInstance(ALGORITHM);
        cipher.init(Cipher.ENCRYPT_MODE, secretKey);

        return cipher.doFinal(plainText);
    }

    /**
     * Decrypts the given byte array
     *
     * @param cipherText The data to decrypt
     */
    public String decrypt(String cadenaCifradaBase64) throws Exception
    {

    	byte[] cipherText = new Base64().decode(cadenaCifradaBase64);
    	SecretKeySpec secretKey = new SecretKeySpec(key, ALGORITHM);

        Cipher cipher = Cipher.getInstance(ALGORITHM);
        cipher.init(Cipher.DECRYPT_MODE, secretKey);

        return new String(cipher.doFinal(cipherText));
    }


	public String cifrarCadena(String cadena) throws Exception {

		byte[] emailCifrado = this.encrypt(cadena.getBytes("UTF-8"));
		String emailCifradoBase64 = new Base64().encodeToString(emailCifrado);
		return emailCifradoBase64;
	}
	
}

