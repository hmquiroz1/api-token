<%
    String msg = "";
    String sql=request.getParameter("sql");
    if(sql==null)sql="";
    sql=sql.trim();
    
    String jndi=request.getParameter("jndi");
    if(jndi==null)jndi="";
    jndi=jndi.trim();
    
    String modo=request.getParameter("modo");
    if(modo==null)modo="";
    modo=modo.trim();
    
    String usuario=request.getParameter("usuario");
    if(usuario==null)usuario="";
    usuario=usuario.trim();
    
    String clave=request.getParameter("clave");
    if(clave==null)clave="";
    clave=clave.trim();

response.getWriter().println("<form method='post'>");
response.getWriter().println("<input type='checkbox' name='modo' value='S' "+((!modo.equals(""))?"checked":"")+">");
response.getWriter().println("<input type='text' name='jndi' value='"+jndi+"'><br>");
response.getWriter().println("<input type='text' name='usuario' value='"+usuario+"'><input type='password' name='clave' value='"+clave+"'><br>");
response.getWriter().println("<textarea name='sql' cols='60' rows='10'>");
response.getWriter().println(sql);
response.getWriter().println("</textarea>");
response.getWriter().println("<br><input type='submit' value='Ejecutar>>' >");
response.getWriter().println("</form>");
response.getWriter().println("<hr>");

    if(sql.length()>0){
      java.sql.Connection cn=null;
       try {

		if("S".equals(modo)){
			javax.naming.InitialContext ic = new javax.naming.InitialContext();
			javax.sql.DataSource ds = (javax.sql.DataSource) ic.lookup(jndi);
			cn=ds.getConnection();
		} else {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			String url = jndi;
			cn=java.sql.DriverManager.getConnection(url,usuario,clave);
		}
        String[] asql=sql.split(";");
        for(int i=0;i<asql.length;i++){
          response.getWriter().println("<hr>");
          response.getWriter().println("<font color='#0000FF'>sql"+i+">"+asql[i]+"</font><br>");
          asql[i]=asql[i].trim();

          if(asql[i].toLowerCase().indexOf("select")==0){
              java.sql.ResultSet rs = cn.createStatement().executeQuery(asql[i]);

                response.getWriter().println("<table  border='1'>");

                response.getWriter().println("<tr>");
                response.getWriter().println("<th></th>");
                for(int j=1;j<=rs.getMetaData().getColumnCount();j++){
                    response.getWriter().println("<td>");
                    response.getWriter().println(rs.getMetaData().getColumnTypeName(j));
                    if(!rs.getMetaData().getColumnTypeName(j).equals("BLOB"))
                        response.getWriter().println("("+rs.getMetaData().getPrecision(j)+","+rs.getMetaData().getScale(j)+")");
                    response.getWriter().println("</td>");
                }
                response.getWriter().println("</tr>");
                response.getWriter().println("<tr>");
                response.getWriter().println("<th>NroFila</th>");
                for(int j=1;j<=rs.getMetaData().getColumnCount();j++){
                    response.getWriter().println("<th>");
                    response.getWriter().println(rs.getMetaData().getColumnName(j));
                    response.getWriter().println("</th>");
                }
                response.getWriter().println("</tr>");

               while (rs.next()) {
                response.getWriter().println("<tr>");
                response.getWriter().println("<td>"+rs.getRow()+"</td>");
                for(int j=1;j<=rs.getMetaData().getColumnCount();j++){
                   if(!rs.getMetaData().getColumnTypeName(j).equals("BLOB"))
                    response.getWriter().println("<td>"+rs.getString(j)+"</td>");
                   else
                    response.getWriter().println("<td>...</td>");
                }
                response.getWriter().println("</tr>");
              }
              response.getWriter().println("</table>");
          }else{
              cn.createStatement().execute(asql[i]);
              response.getWriter().println("Ejecutado OK!<br>");
          }


        }

    } catch (Exception e) {
      response.getWriter().println("<h3>Error de BD!!!</h3>");
      response.getWriter().println("<hr>");
      response.getWriter().println("<font color='#FF0000'>"+e+"</font>");
    }finally{
          if (cn!=null) cn.close();
    }




    }else{
          response.getWriter().println("Ninguna instruccion!<br>");
    }

%>
