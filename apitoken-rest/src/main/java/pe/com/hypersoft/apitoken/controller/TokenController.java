package pe.com.hypersoft.apitoken.controller;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;

import pe.com.hypersoft.apitoken.bean.ObjetoRespuestaMenuOpciones;
import pe.com.hypersoft.apitoken.bean.ObjetoRespuestaUsuario;
import pe.com.hypersoft.apitoken.bean.OcurrenciaBean;
import pe.com.hypersoft.apitoken.bean.UsuarioRestBean;
import pe.com.hypersoft.apitoken.bean.UsuarioSistemaBean;
import pe.com.hypersoft.apitoken.service.JwtService;
import pe.com.hypersoft.apitoken.service.SeguridadService;
import pe.com.hypersoft.apitoken.util.Constante;

@Controller
@RequestMapping(value = "/seguridad")
public class TokenController {
	
	@Autowired
	SeguridadService seguridadService;
		
	@Autowired
	JwtService jwtService;
	
	@RequestMapping(value = "/", method = RequestMethod.POST, headers = "Accept=application/json", produces = "text/plain;charset=UTF-8")
	public @ResponseBody ResponseEntity<Object> registrarSolicitud(@RequestBody Map<String, Object> map) {

		Constante.loggerTokenApi.debug("=== /api-token/rest : inicio ====");
		ObjetoRespuestaUsuario data = new ObjetoRespuestaUsuario();
		ObjectMapper mapper = new ObjectMapper();
		String json = StringUtils.EMPTY;
		try {
			json = mapper.writeValueAsString(map);
		} catch (IOException e) {
			Constante.loggerTokenApi.error(Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR+"al convertir el json de entrada.");
		}catch(Exception e){
			Constante.loggerTokenApi.error(Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR+"al convertir el json de entrada.");
		}

		Constante.loggerTokenApi.info("json : " + json);

		int resp = verificarDatoUsuario(mapper, map);

		if (resp == 1) {

			UsuarioSistemaBean usuarioData = null;
			//TarjetaFPFBean tarjetaFPF = null;
			int idRpta = 0;
			
			if (map.get("tipoServicio") != null) {
				String tipo = (String) map.get("tipoServicio");							
				
				//Consulta para saber si una solicitud ya esta registrada.
				if (tipo.equalsIgnoreCase(Constante.TIPO_OPERACION.CONSULTAR_SI_EXISTE_TOKEN)) {					
					Constante.loggerTokenApi.info("consultarSiExisteToken-entrada: " + json);
					usuarioData = mapper.convertValue(map.get("objeto"), UsuarioSistemaBean.class);
					data = seguridadService.consultarSiExisteToken(usuarioData);					
					idRpta = Integer.parseInt(data.getCodigo());
				}
				
				//Verificacion de rptas
				if (idRpta > 0) {
					data.setCodigo(String.valueOf(idRpta));
					data.setValor(Constante.MENSAJE.MSJ_OK);
				} else if (idRpta == -1) {
					data.setCodigo(Constante.PARAMETRO.COD_RPTA_EXCEPTION);
					data.setValor(Constante.MENSAJE.MSJ_ERROR);
				} else if (idRpta == 0) {
					data.setCodigo(Constante.PARAMETRO.COD_RPTA_NO_EXITO);
					data.setValor(Constante.MENSAJE.MSJ_NO_DATA);
				}
				
				Constante.loggerTokenApi.info("-----------------------------------");
				return new ResponseEntity<Object>(data, HttpStatus.CREATED);
			}

		} else {
			data.setCodigo(Constante.PARAMETRO.COD_RPTA_NO_EXITO);
			data.setValor(Constante.MENSAJE.MSJ_NO_DATA);
			Constante.loggerTokenApi.error("----- salida-ERROR: "+Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR + "al verificarDatoUsuario. ");
			return new ResponseEntity<Object>(data, HttpStatus.CREATED);
		}
		
		Constante.loggerTokenApi.debug("=== /api-token/rest : fin ====");

		return new ResponseEntity<Object>(HttpStatus.BAD_REQUEST);
	}
	
	@RequestMapping(value = "/autenticacion", method = RequestMethod.POST, headers = "Accept=application/json", produces = "text/plain;charset=UTF-8")
	public @ResponseBody ResponseEntity<Object> validarUsuario(HttpServletRequest request,@RequestBody Map<String, Object> map) {

		Constante.loggerUsuarioApi.debug("=== /validarUsuario/ : inicio ====");
		
		HttpHeaders headers = new HttpHeaders();
		//Set Headers
		headers.add("Content-Type", "application/json; charset=UTF-8");		
		String jwtToken;
		ObjetoRespuestaUsuario data = new ObjetoRespuestaUsuario();
		ObjectMapper mapper = new ObjectMapper();
		
		String json = StringUtils.EMPTY;
		try {
			json = mapper.writeValueAsString(map);
		} catch (IOException e) {
			Constante.loggerUsuarioApi.error(Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR+"al convertir el json de entrada.");
		}catch(Exception e){
			Constante.loggerUsuarioApi.error(Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR+"al convertir el json de entrada.");
		}
		
		//int resp = verificarDatoUsuario(mapper, map);

		//if (resp == 1) {
			UsuarioSistemaBean usuarioData = null;
			int idRpta = 0;
				
			Constante.loggerUsuarioApi.info("usuarios-POST: " + json);
			usuarioData = mapper.convertValue(map.get("autenticacion"), UsuarioSistemaBean.class);
			
			if(usuarioData != null){
				if (usuarioData.getEmail() == null || usuarioData.getClave() == null || usuarioData.getEmail().equalsIgnoreCase("") || usuarioData.getClave().equalsIgnoreCase("")) {
					headers.add("Refusal-Motivation", "credenciales invalidas");
					data.setCodigo(Constante.PARAMETRO.COD_RPTA_EXCEPTION);
					data.setValor(Constante.MENSAJE.MSJ_ERROR);
					return new ResponseEntity<Object>(data,headers, HttpStatus.UNAUTHORIZED);
		        }
			}else{
				headers.add("Refusal-Motivation", "credenciales invalidas");
				data.setCodigo(Constante.PARAMETRO.COD_RPTA_EXCEPTION);
				data.setValor(Constante.MENSAJE.MSJ_ERROR);
				return new ResponseEntity<Object>(data,headers, HttpStatus.UNAUTHORIZED);
			}
			
			try {
				data = seguridadService.obtenerUsuario(usuarioData);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			idRpta = Integer.parseInt(data.getCodigo());
		
			//Verificacion de rptas y setear Mensaje
			if (idRpta > 0) {
				jwtToken = jwtService.generateToken(usuarioData.getEmail().trim());
				headers.add("token", jwtToken);
				data.setCodigo(String.valueOf(idRpta));
				data.setValor(Constante.MENSAJE.MSJ_OK);
				data.setToken(jwtToken);
			} else if (idRpta == -1) {
				headers.add("Refusal-Motivation", "credenciales invalidas");
				headers.add("token", null);
				data.setCodigo(Constante.PARAMETRO.COD_RPTA_EXCEPTION);
				data.setValor(Constante.MENSAJE.MSJ_ERROR);
				data.setToken(null);
			} else if (idRpta == 0) {
				headers.add("Refusal-Motivation", "no existe data");
				headers.add("token", null);
				data.setCodigo(Constante.PARAMETRO.COD_RPTA_NO_EXITO);
				data.setValor(Constante.MENSAJE.MSJ_NO_DATA);
				data.setToken(null);
			}				
			Constante.loggerUsuarioApi.debug("=== /validarUsuario/ : fin ====");							
			return new ResponseEntity<Object>(data,headers, HttpStatus.OK);
			
		/*}else {
			data.setCodigo(Constante.PARAMETRO.COD_RPTA_NO_EXITO);
			data.setValor(Constante.MENSAJE.MSJ_NO_DATA);
			Constante.loggerUsuarioApi.error("----- salida-ERROR: "+Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR + "al verificarDatoUsuario. ");
			return new ResponseEntity<Object>(data, HttpStatus.BAD_REQUEST);
		}*/
	}
	
	@RequestMapping(value = "/listarmenu", method = RequestMethod.POST, headers = "Accept=application/json", produces = "text/plain;charset=UTF-8")
	public @ResponseBody ResponseEntity<Object> listarMenu(HttpServletRequest request,@RequestBody Map<String, Object> map) {
	
		Constante.loggerUsuarioApi.debug("=== /listarMenu/ : inicio ====");
		
		HttpHeaders headers = new HttpHeaders();
		//Set Headers
		headers.add("Content-Type", "application/json; charset=UTF-8");	
		
		ObjetoRespuestaMenuOpciones data = new ObjetoRespuestaMenuOpciones();
		ObjectMapper mapper = new ObjectMapper();
		
		String json = StringUtils.EMPTY;
		try {
			json = mapper.writeValueAsString(map);
		} catch (IOException e) {
			Constante.loggerUsuarioApi.error(Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR+"al convertir el json de entrada.");
		}catch(Exception e){
			Constante.loggerUsuarioApi.error(Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR+"al convertir el json de entrada.");
		}
		
		UsuarioSistemaBean usuarioData = null;
		int idRpta = 0;
			
		Constante.loggerUsuarioApi.info("usuarios-POST: " + json);
		usuarioData = mapper.convertValue(map.get("autenticacion"), UsuarioSistemaBean.class);
		
		if (usuarioData.getPerfil() == null) {
			headers.add("Refusal-Motivation", "datos incompletos");
			data.setCodigo(Constante.PARAMETRO.COD_RPTA_EXCEPTION);
			data.setValor(Constante.MENSAJE.MSJ_ERROR);
			return new ResponseEntity<Object>(data,headers, HttpStatus.UNAUTHORIZED);
        }
		
		data = seguridadService.listarMenuOpciones(usuarioData);
		idRpta = Integer.parseInt(data.getCodigo());
	
		//Verificacion de rptas y setear Mensaje
		if (idRpta > 0) {
			data.setCodigo(String.valueOf(idRpta));
			data.setValor(Constante.MENSAJE.MSJ_OK);			
		} else if (idRpta == -1) {
			headers.add("Refusal-Motivation", "credenciales invalidas");
			headers.add("token", null);
			data.setCodigo(Constante.PARAMETRO.COD_RPTA_EXCEPTION);
			data.setValor(Constante.MENSAJE.MSJ_ERROR);			
		} else if (idRpta == 0) {
			headers.add("Refusal-Motivation", "no existe data");
			headers.add("token", null);
			data.setCodigo(Constante.PARAMETRO.COD_RPTA_NO_EXITO);
			data.setValor(Constante.MENSAJE.MSJ_NO_DATA);			
		}	
		
		Constante.loggerUsuarioApi.debug("=== /listarMenu/ : fin ====");							
		return new ResponseEntity<Object>(data,headers, HttpStatus.OK);
	}
	
	//Validacion de usuario generico para consultar servicios gifole-ws
	private int verificarDatoUsuario(ObjectMapper mapper, Map<String, Object> map){		
		UsuarioRestBean usuarioWsBean = mapper.convertValue(map.get("security"), UsuarioRestBean.class);
		byte[] decodedUsuario = Base64.decodeBase64(usuarioWsBean.getUsuario());
		String usuario = new String(decodedUsuario);
		byte[] decodedPassword = Base64.decodeBase64(usuarioWsBean.getPassword());
		String password = new String(decodedPassword);
		
		int resp = seguridadService.autenticarUsuario(usuario, password);
		return resp;
	}

}
