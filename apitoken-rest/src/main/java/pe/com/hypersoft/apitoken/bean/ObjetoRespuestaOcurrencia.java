package pe.com.hypersoft.apitoken.bean;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
public class ObjetoRespuestaOcurrencia implements Serializable{
	
	private String codigo;
	private String valor;
	private List<OcurrenciaBean> listaOcurrencias;	
	
	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}
	
	public String getValor() {
		return valor;
	}

	public List<OcurrenciaBean> getListaOcurrencias() {
		return listaOcurrencias;
	}

	public void setListaOcurrencias(List<OcurrenciaBean> listaOcurrencias) {
		this.listaOcurrencias = listaOcurrencias;
	}	
		
}