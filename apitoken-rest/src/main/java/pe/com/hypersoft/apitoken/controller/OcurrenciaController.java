package pe.com.hypersoft.apitoken.controller;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import pe.com.hypersoft.apitoken.bean.MessageError;
import pe.com.hypersoft.apitoken.bean.ObjetoRespuestaOcurrencia;
import pe.com.hypersoft.apitoken.bean.ObjetoRespuestaUO;
import pe.com.hypersoft.apitoken.bean.ObjetoRespuestaUsuario;
import pe.com.hypersoft.apitoken.bean.OcurrenciaBean;
import pe.com.hypersoft.apitoken.bean.UOSistemaBean;
import pe.com.hypersoft.apitoken.bean.UsuarioRestBean;
import pe.com.hypersoft.apitoken.bean.UsuarioSistemaBean;
import pe.com.hypersoft.apitoken.restful.RestfulOcurrencia;
import pe.com.hypersoft.apitoken.restful.RestfulUO;
import pe.com.hypersoft.apitoken.restful.RestfulUsuario;
import pe.com.hypersoft.apitoken.service.JwtService;
import pe.com.hypersoft.apitoken.service.OcurrenciaService;
import pe.com.hypersoft.apitoken.service.SeguridadService;
import pe.com.hypersoft.apitoken.util.Constante;
import pe.com.hypersoft.apitoken.util.Utilitario;

@RestController
@Controller
@RequestMapping(value = "/ocurrencias")
@Api(value="ocurrenciaController", description="Operaciones para Mantenimiento de Ocurrencias")
public class OcurrenciaController {
	
	@Autowired
	SeguridadService seguridadService;
	
	@Autowired
	OcurrenciaService ocurrenciaService;
	
	@Autowired
	JwtService jwtService;
	
	@ApiOperation(value = "Obtener Lista de Ocurrencias", notes = "Retorna listado ocurrencias por fechas",response = Object.class)
	@ApiResponses(value = {
	        @ApiResponse(code = 200, message = "Ocurrencias obtenidas satisfactoriamente"),
	        @ApiResponse(code = 401, message = "La autentificacion ha fallado o no ha sido provista")
	}
	)
	@RequestMapping(value = "/{uo}/{cc}/", method = RequestMethod.GET, headers = "Accept=application/json", produces = "text/plain;charset=UTF-8")
	public @ResponseBody ResponseEntity<Object> obtenerOcurrencias(@ApiParam("Codigo Unidad Organizativa es obligatorio.")
						@PathVariable("uo") String uo,
						@ApiParam("Codigo Centro de Costo es obligatorio.")@PathVariable("cc") String cc,
						@ApiParam("Map debe contener el objeto security.")@RequestBody Map<String, Object> map) {

		Constante.loggerUsuarioApi.debug("=== /api-token/rest : inicio ====");
		ObjetoRespuestaOcurrencia data = new ObjetoRespuestaOcurrencia();
		RestfulOcurrencia respuestaRest = new RestfulOcurrencia();
		MessageError messageError =  new MessageError();
		
		ObjectMapper mapper = new ObjectMapper();
		String json = StringUtils.EMPTY;
		try {
			json = mapper.writeValueAsString(map);
		} catch (IOException e) {
			Constante.loggerUsuarioApi.error(Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR+"al convertir el json de entrada.");
		}catch(Exception e){
			Constante.loggerUsuarioApi.error(Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR+"al convertir el json de entrada.");
		}

		Constante.loggerUsuarioApi.info("json : " + json);

		int resp = verificarDatoUsuario(mapper, map);

		if (resp == 1) {

			OcurrenciaBean ocuData = new OcurrenciaBean();
			
			int idRpta = 0;		
							
			Constante.loggerUsuarioApi.info("obtenerOcurrencias-entrada: " + json);
			ocuData.setIduo(uo);
			ocuData.setIdcc(cc);
			data = ocurrenciaService.listarOcurrencia(ocuData);
			idRpta = Integer.parseInt(data.getCodigo());
			
			
			//Verificacion de rptas y setear Mensaje
			if (idRpta > 0) {
				data.setCodigo(String.valueOf(idRpta));
				data.setValor(Constante.MENSAJE.MSJ_OK);
			} else if (idRpta == -1) {
				data.setCodigo(Constante.PARAMETRO.COD_RPTA_EXCEPTION);
				data.setValor(Constante.MENSAJE.MSJ_ERROR);
			} else if (idRpta == 0) {
				data.setCodigo(Constante.PARAMETRO.COD_RPTA_NO_EXITO);
				data.setValor(Constante.MENSAJE.MSJ_NO_DATA);
			}
			
			//Set RestFul Response
			respuestaRest.setData(data);
			respuestaRest.setMessageError(null);
			
			Constante.loggerUsuarioApi.info("-----------------------------------");
			return new ResponseEntity<Object>(respuestaRest, HttpStatus.CREATED);			

		} else {
			data.setCodigo(Constante.PARAMETRO.COD_RPTA_NO_EXITO);
			data.setValor(Constante.MENSAJE.MSJ_NO_DATA);
			
			//Set Error Message			
			messageError = Utilitario.sendHttpClientErrors(401);
			
			//Set RestFul Response
			respuestaRest.setData(data);
			respuestaRest.setMessageError(messageError);
			
			Constante.loggerUsuarioApi.error("----- salida-ERROR: "+Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR + "al verificarDatoUsuario. ");
			return new ResponseEntity<Object>(data, messageError.getHttpStatus());
		}		
	}	
		
	@ApiOperation(value = "Registrar Ocurrencia", response = Object.class)
	@ApiResponses(value = {
	        @ApiResponse(code = 200, message = "Ocurrencias registrada satisfactoriamente"),
	        @ApiResponse(code = 401, message = "La autentificacion ha fallado o no ha sido provista")
	}
	)
	@RequestMapping(value = "/", method = RequestMethod.POST, headers = "Accept=application/json", produces = "text/plain;charset=UTF-8")
	public @ResponseBody ResponseEntity<Object> registrarOcurrencia(@RequestBody Map<String, Object> map) {

		Constante.loggerUsuarioApi.debug("=== /ocurrencias/ : inicio ====");
		ObjetoRespuestaOcurrencia data = new ObjetoRespuestaOcurrencia();
		RestfulOcurrencia respuestaRest = new RestfulOcurrencia();
		MessageError messageError =  new MessageError();
		
		ObjectMapper mapper = new ObjectMapper();
		String json = StringUtils.EMPTY;
		try {
			json = mapper.writeValueAsString(map);
		} catch (IOException e) {
			Constante.loggerUsuarioApi.error(Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR+"al convertir el json de entrada.");
		}catch(Exception e){
			Constante.loggerUsuarioApi.error(Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR+"al convertir el json de entrada.");
		}

		Constante.loggerUsuarioApi.info("json : " + json);

		int resp = verificarDatoUsuario(mapper, map);

		if (resp == 1) {

			OcurrenciaBean ocurrData = null;
			
			int idRpta = 0;			
								
			//Agregar un Usuario.						
			Constante.loggerUsuarioApi.info("registrarOcurrencia-entrada: " + json);
			ocurrData = mapper.convertValue(map.get("objeto"), OcurrenciaBean.class);
			data = ocurrenciaService.agregarOcurrencia(ocurrData);
			idRpta = Integer.parseInt(data.getCodigo());
						
			
			//Verificacion de rptas y setear Mensaje
			if (idRpta > 0) {
				data.setCodigo(String.valueOf(idRpta));
				data.setValor(Constante.MENSAJE.MSJ_OK);
			} else if (idRpta == -1) {
				data.setCodigo(Constante.PARAMETRO.COD_RPTA_EXCEPTION);
				data.setValor(Constante.MENSAJE.MSJ_ERROR);
			} else if (idRpta == 0) {
				data.setCodigo(Constante.PARAMETRO.COD_RPTA_NO_EXITO);
				data.setValor(Constante.MENSAJE.MSJ_NO_DATA);
			}
			
			//Set RestFul Response
			respuestaRest.setData(data);
			respuestaRest.setMessageError(null);
			Constante.loggerUsuarioApi.info("-----------------------------------");
			return new ResponseEntity<Object>(respuestaRest, HttpStatus.CREATED);			

		} else {
			data.setCodigo(Constante.PARAMETRO.COD_RPTA_NO_EXITO);
			data.setValor(Constante.MENSAJE.MSJ_NO_DATA);
			
			//Set Error Message			
			messageError = Utilitario.sendHttpClientErrors(401);
			
			//Set RestFul Response
			respuestaRest.setData(data);
			respuestaRest.setMessageError(messageError);
			
			Constante.loggerUsuarioApi.error("----- salida-ERROR: "+Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR + "al registrar usuario. ");
			return new ResponseEntity<Object>(respuestaRest, messageError.getHttpStatus());
		}			
		
	}
	
	@ApiOperation(value = "Actualizar Ocurrencia", response = Object.class)
	@ApiResponses(value = {
	        @ApiResponse(code = 200, message = "Ocurrencia actualizada satisfactoriamente"),
	        @ApiResponse(code = 401, message = "La autentificacion ha fallado o no ha sido provista")
	}
	)
	@RequestMapping(value = "/", method = RequestMethod.PUT, headers = "Accept=application/json", produces = "text/plain;charset=UTF-8")
	public @ResponseBody ResponseEntity<Object> actualizarOcurrencia(@RequestBody Map<String, Object> map) {

		Constante.loggerUsuarioApi.debug("=== /ocurrencias/ : inicio ====");
		ObjetoRespuestaOcurrencia data = new ObjetoRespuestaOcurrencia();
		RestfulOcurrencia respuestaRest = new RestfulOcurrencia();
		MessageError messageError =  new MessageError();
		
		ObjectMapper mapper = new ObjectMapper();
		String json = StringUtils.EMPTY;
		try {
			json = mapper.writeValueAsString(map);
		} catch (IOException e) {
			Constante.loggerUsuarioApi.error(Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR+"al convertir el json de entrada.");
		}catch(Exception e){
			Constante.loggerUsuarioApi.error(Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR+"al convertir el json de entrada.");
		}

		Constante.loggerUsuarioApi.info("json : " + json);

		int resp = verificarDatoUsuario(mapper, map);

		if (resp == 1) {

			OcurrenciaBean ocurrData = null;
			
			int idRpta = 0;			
								
			//Actualizar un Usuario.						
			Constante.loggerUsuarioApi.info("actualizarOcurrencia-entrada: " + json);
			ocurrData = mapper.convertValue(map.get("objeto"), OcurrenciaBean.class);
			data = ocurrenciaService.actualizarOcurrencia(ocurrData);
			idRpta = Integer.parseInt(data.getCodigo());
						
			
			//Verificacion de rptas y setear Mensaje
			if (idRpta > 0) {
				data.setCodigo(String.valueOf(idRpta));
				data.setValor(Constante.MENSAJE.MSJ_OK);
			} else if (idRpta == -1) {
				data.setCodigo(Constante.PARAMETRO.COD_RPTA_EXCEPTION);
				data.setValor(Constante.MENSAJE.MSJ_ERROR);
			} else if (idRpta == 0) {
				data.setCodigo(Constante.PARAMETRO.COD_RPTA_NO_EXITO);
				data.setValor(Constante.MENSAJE.MSJ_NO_DATA);
			}
			
			Constante.loggerUsuarioApi.info("-----------------------------------");
			return new ResponseEntity<Object>(data, HttpStatus.CREATED);			

		} else {
			data.setCodigo(Constante.PARAMETRO.COD_RPTA_NO_EXITO);
			data.setValor(Constante.MENSAJE.MSJ_NO_DATA);
			
			//Set Error Message			
			messageError = Utilitario.sendHttpClientErrors(401);
			
			//Set RestFul Response
			respuestaRest.setData(data);
			respuestaRest.setMessageError(messageError);
			
			Constante.loggerUsuarioApi.error("----- salida-ERROR: "+Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR + "al actualizar ocurrencia. ");
			return new ResponseEntity<Object>(data, messageError.getHttpStatus());
		}			
		
	}
	
	@ApiOperation(value = "Eliminar Ocurrencia", response = Object.class)
	@ApiResponses(value = {
	        @ApiResponse(code = 200, message = "Ocurrencia eliminada satisfactoriamente"),
	        @ApiResponse(code = 401, message = "La autentificacion ha fallado o no ha sido provista")
	}
	)
	@RequestMapping(value = "/", method = RequestMethod.DELETE, headers = "Accept=application/json", produces = "text/plain;charset=UTF-8")
	public @ResponseBody ResponseEntity<Object> eliminarOcurrencia(@RequestBody Map<String, Object> map) {

		Constante.loggerUsuarioApi.debug("=== /usuarios/ : inicio ====");
		ObjetoRespuestaOcurrencia data = new ObjetoRespuestaOcurrencia();
		RestfulOcurrencia respuestaRest = new RestfulOcurrencia();
		MessageError messageError =  new MessageError();
		
		ObjectMapper mapper = new ObjectMapper();
		String json = StringUtils.EMPTY;
		try {
			json = mapper.writeValueAsString(map);
		} catch (IOException e) {
			Constante.loggerUsuarioApi.error(Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR+"al convertir el json de entrada.");
		}catch(Exception e){
			Constante.loggerUsuarioApi.error(Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR+"al convertir el json de entrada.");
		}

		Constante.loggerUsuarioApi.info("json : " + json);

		int resp = verificarDatoUsuario(mapper, map);

		if (resp == 1) {

			OcurrenciaBean ocurrData = null;
			
			int idRpta = 0;			
								
			//Eliminar un Usuario.						
			Constante.loggerUsuarioApi.info("eliminarOcurrencia-entrada: " + json);
			ocurrData = mapper.convertValue(map.get("objeto"), OcurrenciaBean.class);
			data = ocurrenciaService.eliminarOcurrencia(ocurrData);
			idRpta = Integer.parseInt(data.getCodigo());
			
			//Verificacion de rptas y setear Mensaje
			if (idRpta > 0) {
				data.setCodigo(String.valueOf(idRpta));
				data.setValor(Constante.MENSAJE.MSJ_OK);
			} else if (idRpta == -1) {
				data.setCodigo(Constante.PARAMETRO.COD_RPTA_EXCEPTION);
				data.setValor(Constante.MENSAJE.MSJ_ERROR);
			} else if (idRpta == 0) {
				data.setCodigo(Constante.PARAMETRO.COD_RPTA_NO_EXITO);
				data.setValor(Constante.MENSAJE.MSJ_NO_DATA);
			}
			
			Constante.loggerUsuarioApi.info("-----------------------------------");
			return new ResponseEntity<Object>(data, HttpStatus.CREATED);			

		} else {
			data.setCodigo(Constante.PARAMETRO.COD_RPTA_NO_EXITO);
			data.setValor(Constante.MENSAJE.MSJ_NO_DATA);
			
			//Set Error Message			
			messageError = Utilitario.sendHttpClientErrors(401);
			
			//Set RestFul Response
			respuestaRest.setData(data);
			respuestaRest.setMessageError(messageError);
			
			Constante.loggerUsuarioApi.error("----- salida-ERROR: "+Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR + "al eliminar ocurrencia. ");
			return new ResponseEntity<Object>(data, messageError.getHttpStatus());
		}			
		
	}
	
	
	//Validacion de usuario generico para metodo POST
	private int verificarDatoUsuario(ObjectMapper mapper, Map<String, Object> map){		
		UsuarioRestBean usuarioWsBean = mapper.convertValue(map.get("security"), UsuarioRestBean.class);
		byte[] decodedUsuario = Base64.decodeBase64(usuarioWsBean.getUsuario());
		String usuario = new String(decodedUsuario);
		byte[] decodedPassword = Base64.decodeBase64(usuarioWsBean.getPassword());
		String password = new String(decodedPassword);
		
		int resp = seguridadService.autenticarUsuario(usuario, password);
		return resp;
	}	

}
