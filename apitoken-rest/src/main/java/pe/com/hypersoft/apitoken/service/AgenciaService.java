package pe.com.hypersoft.apitoken.service;

import pe.com.hypersoft.apitoken.bean.AgenciaBean;
import pe.com.hypersoft.apitoken.bean.ObjetoRespuestaAgencia;
import pe.com.hypersoft.apitoken.bean.ObjetoRespuestaOcurrencia;
import pe.com.hypersoft.apitoken.bean.OcurrenciaBean;

public interface AgenciaService {		
	
	ObjetoRespuestaAgencia listarAgencia(AgenciaBean agenciaBean);	
	String registrarAgencia(AgenciaBean agenciaBean);	
}