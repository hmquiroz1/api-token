package pe.com.hypersoft.apitoken.bean;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
public class ObjetoRespuestaParametroUnit implements Serializable{
	
	private String codigo;
	private String valor;
	private List<CmbParametroBean> listaCombo;
	
	
	public List<CmbParametroBean> getListaCombo() {
		return listaCombo;
	}

	public void setListaCombo(List<CmbParametroBean> listaCombo) {
		this.listaCombo = listaCombo;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}
	
	public String getValor() {
		return valor;
	}	
		
}