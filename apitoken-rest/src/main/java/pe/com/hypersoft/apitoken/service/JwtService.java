package pe.com.hypersoft.apitoken.service;

public interface JwtService {
	
	String generateToken(String username);
	String verifyToken(String token);
}
