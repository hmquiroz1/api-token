package pe.com.hypersoft.apitoken.restful;

import java.io.Serializable;
import java.util.List;

import pe.com.hypersoft.apitoken.bean.MessageError;
import pe.com.hypersoft.apitoken.bean.ObjetoRespuestaParametroAll;
import pe.com.hypersoft.apitoken.bean.ObjetoRespuestaUO;
import pe.com.hypersoft.apitoken.bean.ObjetoRespuestaUsuario;

@SuppressWarnings("serial")
public class RestfulParametroAll implements Serializable{
	
	private ObjetoRespuestaParametroAll response;
	private MessageError messageError;
	
	public ObjetoRespuestaParametroAll getData() {
		return response;
	}
	public void setData(ObjetoRespuestaParametroAll data) {
		this.response = data;
	}
	public MessageError getMessageError() {
		return messageError;
	}
	public void setMessageError(MessageError messageError) {
		this.messageError = messageError;
	}
		
}