package pe.com.hypersoft.apitoken.restful;

import java.io.Serializable;
import java.util.List;

import pe.com.hypersoft.apitoken.bean.MessageError;
import pe.com.hypersoft.apitoken.bean.ObjetoRespuestaUsuario;

@SuppressWarnings("serial")
public class RestfulUsuario implements Serializable{
	
	private ObjetoRespuestaUsuario response;
	private MessageError messageError;
	
	public ObjetoRespuestaUsuario getData() {
		return response;
	}
	public void setData(ObjetoRespuestaUsuario data) {
		this.response = data;
	}
	public MessageError getMessageError() {
		return messageError;
	}
	public void setMessageError(MessageError messageError) {
		this.messageError = messageError;
	}
		
}