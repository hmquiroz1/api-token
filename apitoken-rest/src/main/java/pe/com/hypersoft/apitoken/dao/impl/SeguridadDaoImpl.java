package pe.com.hypersoft.apitoken.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import pe.com.hypersoft.apitoken.bean.PerfilOpcion;
import pe.com.hypersoft.apitoken.bean.UsuarioRestBean;
import pe.com.hypersoft.apitoken.dao.SeguridadDao;
import pe.com.hypersoft.apitoken.dao.mapper.PerfilOpcionMapper;
import pe.com.hypersoft.apitoken.dao.mapper.UsuarioRestRowMapper;
import pe.com.hypersoft.apitoken.dao.mapper.UsuarioSistemaRowMapper;
import pe.com.hypersoft.apitoken.util.Funcion;
import pe.com.hypersoft.apitoken.util.Utilitario;

import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import pe.com.hypersoft.apitoken.bean.UsuarioSistemaBean;

@Repository
public class SeguridadDaoImpl implements SeguridadDao {

	@Autowired
	JdbcTemplate jdbcTemplate;	
	
	@Autowired
	private NamedParameterJdbcTemplate namedJdbcTemplate;
	
	@Autowired
	UsuarioSistemaRowMapper usuarioSistemaRowMapper;
	
	@Autowired
	PerfilOpcionMapper perfilOpcionMapper;
	 
	@Value("${cifrado.token.key}")
	private String claveCifrado;
	
	@SuppressWarnings("unchecked")
	public int autenticarUsuario(String usuario, String password) {
		Integer id = 0;
		String sql = "SELECT U.ID_USUARIO AS ID FROM APP_SEGURIDAD.USUARIO U WHERE TRIM(U.REGISTRO) = '" + usuario + "' AND TRIM(U.CLAVE) = '" + password + "' AND U.ESTADO='A'";
		UsuarioRestBean param = (UsuarioRestBean) jdbcTemplate.queryForObject(
		sql, new Object[] {}, new UsuarioRestRowMapper());
		if(param!=null){
			if(param.getId().intValue()>0)
				id=1;
		}
		return id;
	}
	
	@SuppressWarnings("unchecked")
	public List<UsuarioSistemaBean> consultarSiExisteToken(UsuarioSistemaBean usuarioSistemaBean) {
		StringBuilder sql=new StringBuilder();
		sql.append("SELECT ID_USUARIO,EMAIL,ESTADO  FROM APP_SEGURIDAD.USUARIO  ");
		sql.append(" WHERE API_TOKEN = '" + usuarioSistemaBean.getToken() + "' ");
		return jdbcTemplate.query(sql.toString(), usuarioSistemaRowMapper);
	}
	
	@SuppressWarnings("unchecked")
	public List<UsuarioSistemaBean> obtenerUsuario(UsuarioSistemaBean usuarioSistemaBean) {		
		StringBuilder sql=new StringBuilder();
		sql.append("SELECT A.ID_USUARIO , A.REGISTRO, A.NOMBRE, A.PATERNO, A.MATERNO, A.TIPODOC,A.NRODOC,FECNACIMIENTO,A.ESTADOCIVIL,A.EMAIL,A.TELEFONO,A.ESTADO,A.API_TOKEN,U.DESCRIPCION AS NOMEDIFICIO,A.ID_PERFIL AS PERFIL,C.IDCC AS ID_CC,C.ID_UO AS ID_UO FROM APP_SEGURIDAD.USUARIO A,APP_SEGURIDAD.CENTRO_COSTO C,APP_SEGURIDAD.UO U ");		
		sql.append("WHERE  A.IDCC = C.IDCC and A.ID_UO = C.ID_UO and C.ID_UO = U.ID_UO AND ");
		sql.append(Utilitario.manejoCondicionalConsultaSQL(usuarioSistemaBean.getEmail(), "A.EMAIL") + " AND ");
		sql.append(Utilitario.manejoCondicionalConsultaSQL(usuarioSistemaBean.getClave(), "A.CLAVE") + " AND ");
		sql.append(Utilitario.manejoCondicionalConsultaSQL("A", "A.ESTADO"));
				
		return jdbcTemplate.query(sql.toString(), usuarioSistemaRowMapper);
	}
	
	public int agregarUsuario(UsuarioSistemaBean usuarioSistemaBean) {	
		int rows = 0;
		String sql1 = "";
		sql1  = sql1 + "INSERT INTO APP_SEGURIDAD.USUARIO (REGISTRO,NOMBRE,PATERNO,MATERNO,TIPODOC,NRODOC,FECNACIMIENTO,ESTADOCIVIL,EMAIL,TELEFONO,ESTADO,CLAVE, ID_PERFIL,IDCC,ID_UO,API_TOKEN) ";
		sql1  = sql1 + "VALUES (:registro,:nombre,:paterno,:materno,:tipodoc,:nrodoc,:fecnac,:estadoCivil,:email,:telefono,:estado,:clave,:idPerfil,:idCC,:idUO,:token) "; 
		
		SqlParameterSource namedParameters = new MapSqlParameterSource();		
		
		if(usuarioSistemaBean.getRegistro() != null) ((MapSqlParameterSource) namedParameters).addValue("registro", usuarioSistemaBean.getRegistro());
		if(usuarioSistemaBean.getNombre() != null) ((MapSqlParameterSource) namedParameters).addValue("nombre", usuarioSistemaBean.getNombre());
		if(usuarioSistemaBean.getPaterno() != null) ((MapSqlParameterSource) namedParameters).addValue("paterno", usuarioSistemaBean.getPaterno());
		if(usuarioSistemaBean.getMaterno() != null) ((MapSqlParameterSource) namedParameters).addValue("materno", usuarioSistemaBean.getMaterno());
		if(usuarioSistemaBean.getTipoDoc() != null) ((MapSqlParameterSource) namedParameters).addValue("tipodoc", usuarioSistemaBean.getTipoDoc());
		if(usuarioSistemaBean.getNroDoc() != null) ((MapSqlParameterSource) namedParameters).addValue("nrodoc", usuarioSistemaBean.getNroDoc());		
		if(usuarioSistemaBean.getFecNacimiento() != null) ((MapSqlParameterSource) namedParameters).addValue("fecnac", Funcion.stringToDate(usuarioSistemaBean.getFecNacimiento(), "dd/MM/yyyy"), Types.DATE);
		if(usuarioSistemaBean.getEstadoCivil() != null) ((MapSqlParameterSource) namedParameters).addValue("estadoCivil", usuarioSistemaBean.getEstadoCivil());	
		if(usuarioSistemaBean.getEmail() != null) ((MapSqlParameterSource) namedParameters).addValue("email", usuarioSistemaBean.getEmail());
		if(usuarioSistemaBean.getTelefono() != null) ((MapSqlParameterSource) namedParameters).addValue("telefono", usuarioSistemaBean.getTelefono());
		if(usuarioSistemaBean.getEstado() != null) ((MapSqlParameterSource) namedParameters).addValue("estado", usuarioSistemaBean.getEstado());
		if(usuarioSistemaBean.getClave() != null) ((MapSqlParameterSource) namedParameters).addValue("clave", usuarioSistemaBean.getClave());
		if(usuarioSistemaBean.getPerfil() != null) ((MapSqlParameterSource) namedParameters).addValue("idPerfil", usuarioSistemaBean.getPerfil());
		if(usuarioSistemaBean.getIdcc() != null) ((MapSqlParameterSource) namedParameters).addValue("idCC", usuarioSistemaBean.getIdcc());
		if(usuarioSistemaBean.getIduo() != null) ((MapSqlParameterSource) namedParameters).addValue("idUO", usuarioSistemaBean.getIduo());
		((MapSqlParameterSource) namedParameters).addValue("token", Utilitario.cifrarToken(claveCifrado, usuarioSistemaBean.getEmail()));
		
		rows = namedJdbcTemplate.update(sql1, namedParameters);	
				
		return rows;
	}
	
	public int actualizarUsuario(UsuarioSistemaBean usuarioSistemaBean) {	
		int rows = 0;
		String sql1 = "";
		sql1  = sql1 + "UPDATE APP_SEGURIDAD.USUARIO SET ";
		
		if(usuarioSistemaBean.getNombre() != null) sql1 = sql1 + " NOMBRE = :nombre,";
		if(usuarioSistemaBean.getPaterno() != null) sql1 = sql1 + " PATERNO = :paterno,";
		if(usuarioSistemaBean.getMaterno() != null) sql1 = sql1 + " MATERNO = :materno,";
		if(usuarioSistemaBean.getMaterno() != null) sql1 = sql1 + " FECNACIMIENTO = :fecnac,";
		if(usuarioSistemaBean.getEmail() != null) sql1 = sql1 + " EMAIL = :email,";
		if(usuarioSistemaBean.getTelefono() != null) sql1 = sql1 + " TELEFONO = :telefono,";
		if(usuarioSistemaBean.getTipoDoc() != null) sql1 = sql1 + " TIPODOC = :tipodoc,";
		if(usuarioSistemaBean.getEstado() != null) sql1 = sql1 + " ESTADO = :estado,";
		if(usuarioSistemaBean.getNroDoc() != null){ sql1 = sql1 + " NRODOC = :nrodoc ";} else{ sql1 = sql1.substring(0, sql1.length()-1);}
		
		sql1 = sql1 + " WHERE ID_USUARIO = :id ";
		
		SqlParameterSource namedParameters = new MapSqlParameterSource();		
		
		if(usuarioSistemaBean.getNombre() != null) ((MapSqlParameterSource) namedParameters).addValue("nombre", usuarioSistemaBean.getNombre());
		if(usuarioSistemaBean.getPaterno() != null) ((MapSqlParameterSource) namedParameters).addValue("paterno", usuarioSistemaBean.getPaterno());
		if(usuarioSistemaBean.getMaterno() != null) ((MapSqlParameterSource) namedParameters).addValue("materno", usuarioSistemaBean.getMaterno());
		if(usuarioSistemaBean.getFecNacimiento() != null) ((MapSqlParameterSource) namedParameters).addValue("fecnac", Funcion.stringToDate(usuarioSistemaBean.getFecNacimiento(), "dd/MM/yyyy"), Types.DATE);
		if(usuarioSistemaBean.getEmail() != null) ((MapSqlParameterSource) namedParameters).addValue("email", usuarioSistemaBean.getEmail());
		if(usuarioSistemaBean.getTelefono() != null) ((MapSqlParameterSource) namedParameters).addValue("telefono", usuarioSistemaBean.getTelefono());
		if(usuarioSistemaBean.getEstado() != null) ((MapSqlParameterSource) namedParameters).addValue("estado", usuarioSistemaBean.getEstado());
		if(usuarioSistemaBean.getTipoDoc() != null) ((MapSqlParameterSource) namedParameters).addValue("tipodoc", usuarioSistemaBean.getTipoDoc());
		if(usuarioSistemaBean.getNroDoc() != null) ((MapSqlParameterSource) namedParameters).addValue("nrodoc", usuarioSistemaBean.getNroDoc());
				
		((MapSqlParameterSource) namedParameters).addValue("id", usuarioSistemaBean.getId());			
		
		rows = namedJdbcTemplate.update(sql1, namedParameters);	
				
		return rows;
	}
	
	@SuppressWarnings("unchecked")
	public int actualizarUsuarioCampos(UsuarioSistemaBean usuarioSistemaBean) {	
		int rows = 0;	
		
		String  sql="UPDATE APP_SEGURIDAD.USUARIO SET NOMBRE = ?, PATERNO = ? , MATERNO = ?, TIPODOC = ? , NRODOC = ? WHERE ID_USUARIO = ?";
		
		Object[] params = {usuarioSistemaBean.getNombre(), usuarioSistemaBean.getPaterno(), usuarioSistemaBean.getMaterno(),usuarioSistemaBean.getTipoDoc(),usuarioSistemaBean.getNroDoc(),usuarioSistemaBean.getId()};
        int[] types = {Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.BIGINT};       
		
        rows = jdbcTemplate.update(sql.toString(), params, types);
				
		return rows;
	}
	
	@SuppressWarnings("unchecked")
	public int eliminarUsuario(UsuarioSistemaBean usuarioSistemaBean) {		
		StringBuilder sql=new StringBuilder();
		sql.append("DELETE FROM APP_SEGURIDAD.USUARIO WHERE ID_USUARIO = " + usuarioSistemaBean.getId());		
				
		return jdbcTemplate.update(sql.toString());
	}
	
	public List<PerfilOpcion> listarMenuOpciones(UsuarioSistemaBean usuarioSistemaBean) {		
		StringBuilder sql=new StringBuilder();
		sql.append("select PO.ID_OPCION,P.DESCRIPCION AS DESCPERFIL,TRIM(O.DESCRIPCION) AS DESCOPCION,TRIM(O.LINK) AS LINK,TRIM(O.ICONO) AS ICONO,O.IND_NEW AS NEW,IFNULL(O.PADRE,0) AS PADRE , O.ORDEN AS ORDEN from APP_SEGURIDAD.PERFIL_OPCION PO,APP_SEGURIDAD.OPCION O,APP_SEGURIDAD.PERFIL P ");		
		sql.append("where PO.ID_OPCION = O.ID_OPCION AND PO.ID_PERFIL = P.ID_PERFIL AND O.ESTADO = 'A' AND P.ESTADO = 'A' AND PO.ESTADO = 'A' AND ");
		sql.append("PO.ID_PERFIL = " + Long.parseLong(usuarioSistemaBean.getPerfil()) + " ");
		sql.append("order by ORDEN ASC ");
				
		return jdbcTemplate.query(sql.toString(), perfilOpcionMapper);
	}
}
