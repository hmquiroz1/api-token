package pe.com.hypersoft.apitoken.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import pe.com.hypersoft.apitoken.bean.ObjetoRespuestaOcurrencia;
import pe.com.hypersoft.apitoken.bean.ObjetoRespuestaUO;
import pe.com.hypersoft.apitoken.bean.ObjetoRespuestaUsuario;
import pe.com.hypersoft.apitoken.bean.OcurrenciaBean;
import pe.com.hypersoft.apitoken.bean.PerfilOpcion;
import pe.com.hypersoft.apitoken.bean.UOSistemaBean;
import pe.com.hypersoft.apitoken.bean.UsuarioSistemaBean;
import pe.com.hypersoft.apitoken.dao.OcurrenciaDao;
import pe.com.hypersoft.apitoken.dao.SeguridadDao;
import pe.com.hypersoft.apitoken.service.OcurrenciaService;
import pe.com.hypersoft.apitoken.service.SeguridadService;
import pe.com.hypersoft.apitoken.util.Constante;

@Service
public class OcurrenciaServiceImpl implements OcurrenciaService {
	
	@Autowired
	OcurrenciaDao ocurrenciaDao;
		
	
	public ObjetoRespuestaOcurrencia listarOcurrencia(OcurrenciaBean ocurrenciaBean) {
		Constante.loggerTokenApi.info("========== INICIO listarOcurrencias ==========");		
		ObjetoRespuestaOcurrencia data = new ObjetoRespuestaOcurrencia();		
		List<OcurrenciaBean> listaOcurrencias= new ArrayList<OcurrenciaBean>();
		
		try {			
			listaOcurrencias = ocurrenciaDao.listarOcurrencias(ocurrenciaBean);	
			if(listaOcurrencias != null){
				if(listaOcurrencias.size() > 0){				
					Constante.loggerTokenApi.info("Hay: "+listaOcurrencias.size() + " registros encontrados.");
					data.setListaOcurrencias(listaOcurrencias);
					data.setCodigo(Constante.PARAMETRO.COD_RPTA_EXITO);
				} else {
					data.setCodigo(Constante.PARAMETRO.COD_RPTA_NO_EXITO);
				}
			}else{
				data.setCodigo(Constante.PARAMETRO.COD_RPTA_NO_EXITO);
			}
		} catch (Exception e) {
			data.setCodigo(Constante.PARAMETRO.COD_RPTA_EXCEPTION);
			Constante.loggerTokenApi.error(Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR+"al consultar si existe ocurrencias registradas: " + e);
		}
		Constante.loggerTokenApi.info("========== FIN listarOcurrencias ==========");
		return data;
	}
	
	public ObjetoRespuestaOcurrencia agregarOcurrencia(OcurrenciaBean ocurrenciaBean) {
		Constante.loggerTokenApi.info("========== INICIO agregarUO ==========");		
		ObjetoRespuestaOcurrencia data = new ObjetoRespuestaOcurrencia();		
		int respuesta = 0;
		
		try {			
			respuesta = ocurrenciaDao.agregarOcurrencia(ocurrenciaBean);	
			
			if(respuesta == 1){			
				Constante.loggerTokenApi.info("Se agregó la Ocurrencia: "+ocurrenciaBean.getDesccorta().toString());				
				data.setCodigo(Constante.PARAMETRO.COD_RPTA_EXITO);
			} else {
				data.setCodigo(Constante.PARAMETRO.COD_RPTA_NO_EXITO);
			}
			
		} catch (Exception e) {
			data.setCodigo(Constante.PARAMETRO.COD_RPTA_EXCEPTION);
			Constante.loggerTokenApi.error(Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR+"al consultar si existe solicitud registrada: " + e);
		}
		Constante.loggerTokenApi.info("========== FIN agregarUO ==========");
		return data;
	}
	
	public ObjetoRespuestaOcurrencia obtenerOcurrencia(OcurrenciaBean ocurrenciaBean) {
		Constante.loggerTokenApi.info("========== INICIO obtenerOcurrencia ==========");		
		ObjetoRespuestaOcurrencia data = new ObjetoRespuestaOcurrencia();		
		List<OcurrenciaBean> listaOcur = new ArrayList<OcurrenciaBean>();
		
		try {			
			listaOcur = ocurrenciaDao.obtenerOcurrencia(ocurrenciaBean);	
			if(listaOcur != null){
				if(listaOcur.size() > 0){				
					Constante.loggerTokenApi.info("Hay: "+listaOcur.size() + " registros encontrados.");
					data.setListaOcurrencias(listaOcur);
					data.setCodigo(Constante.PARAMETRO.COD_RPTA_EXITO);
				} else {
					data.setCodigo(Constante.PARAMETRO.COD_RPTA_NO_EXITO);
				}
			}else{
				data.setCodigo(Constante.PARAMETRO.COD_RPTA_NO_EXITO);
			}
		} catch (Exception e) {
			data.setCodigo(Constante.PARAMETRO.COD_RPTA_EXCEPTION);
			Constante.loggerTokenApi.error(Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR+"al consultar si existe solicitud registrada: " + e);
		}
		Constante.loggerTokenApi.info("========== FIN obtenerOcurrencia ==========");
		return data;
	}
	
	public ObjetoRespuestaOcurrencia actualizarOcurrencia(OcurrenciaBean ocurrenciaBean) {
		Constante.loggerTokenApi.info("========== INICIO actualizarOcurrencia ==========");		
		ObjetoRespuestaOcurrencia data = new ObjetoRespuestaOcurrencia();		
		int respuesta = 0;
		
		try {			
			respuesta = ocurrenciaDao.actualizarOcurrencia(ocurrenciaBean);	
		
			if(respuesta == 1){				
				Constante.loggerTokenApi.info("Se actualizó la Ocurrencia: "+ocurrenciaBean.getIdOcurr().toString());				
				data.setCodigo(Constante.PARAMETRO.COD_RPTA_EXITO);
			} else {
				data.setCodigo(Constante.PARAMETRO.COD_RPTA_NO_EXITO);
			}
			
		} catch (Exception e) {
			data.setCodigo(Constante.PARAMETRO.COD_RPTA_EXCEPTION);
			Constante.loggerTokenApi.error(Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR+"al consultar si existe solicitud registrada: " + e);
		}
		Constante.loggerTokenApi.info("========== FIN actualizarUOo ==========");
		return data;
	}
	
	public ObjetoRespuestaOcurrencia eliminarOcurrencia(OcurrenciaBean ocurrenciaBean) {
		Constante.loggerTokenApi.info("========== INICIO eliminarOcurrencia ==========");		
		ObjetoRespuestaOcurrencia data = new ObjetoRespuestaOcurrencia();		
		int respuesta = 0;
		
		try {			
			respuesta = ocurrenciaDao.eliminarOcurrencia(ocurrenciaBean);	
		
			if(respuesta == 1){				
				Constante.loggerTokenApi.info("Se eliminó la Ocurrencia: "+ocurrenciaBean.getIdOcurr().toString());				
				data.setCodigo(Constante.PARAMETRO.COD_RPTA_EXITO);
			} else {
				data.setCodigo(Constante.PARAMETRO.COD_RPTA_NO_EXITO);
			}
			
		} catch (Exception e) {
			data.setCodigo(Constante.PARAMETRO.COD_RPTA_EXCEPTION);
			Constante.loggerTokenApi.error(Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR+"al consultar si existe solicitud registrada: " + e);
		}
		Constante.loggerTokenApi.info("========== FIN eliminarOcurrencia ==========");
		return data;
	}
}
