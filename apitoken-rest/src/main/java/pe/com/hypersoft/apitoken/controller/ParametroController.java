package pe.com.hypersoft.apitoken.controller;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;

import pe.com.hypersoft.apitoken.bean.MessageError;
import pe.com.hypersoft.apitoken.bean.ObjetoRespuestaParametroAll;
import pe.com.hypersoft.apitoken.bean.ObjetoRespuestaParametroUnit;
import pe.com.hypersoft.apitoken.bean.ObjetoRespuestaUO;
import pe.com.hypersoft.apitoken.bean.ObjetoRespuestaUsuario;
import pe.com.hypersoft.apitoken.bean.ParametroBean;
import pe.com.hypersoft.apitoken.bean.UOSistemaBean;
import pe.com.hypersoft.apitoken.bean.UsuarioRestBean;
import pe.com.hypersoft.apitoken.bean.UsuarioSistemaBean;
import pe.com.hypersoft.apitoken.restful.RestfulParametroAll;
import pe.com.hypersoft.apitoken.restful.RestfulParametroUnit;
import pe.com.hypersoft.apitoken.restful.RestfulUO;
import pe.com.hypersoft.apitoken.restful.RestfulUsuario;
import pe.com.hypersoft.apitoken.service.JwtService;
import pe.com.hypersoft.apitoken.service.ParametroService;
import pe.com.hypersoft.apitoken.service.SeguridadService;
import pe.com.hypersoft.apitoken.service.UOService;
import pe.com.hypersoft.apitoken.util.Constante;
import pe.com.hypersoft.apitoken.util.Utilitario;

@Controller
@RequestMapping(value = "/sigri/parametro")
public class ParametroController {
	
	@Autowired
	SeguridadService seguridadService;
	
	@Autowired
	UOService uoService;
	
	@Autowired
	ParametroService parametroService;
	
	@Autowired
	JwtService jwtService;
	
	@RequestMapping(value = "/{tipo}", method = RequestMethod.GET, headers = "Accept=application/json", produces = "text/plain;charset=UTF-8")
	public @ResponseBody ResponseEntity<Object> obtenerParametro(@PathVariable("tipo") String tipo,@RequestBody Map<String, Object> map) {

		Constante.loggerUsuarioApi.debug("=== /obtenerParametro/rest : inicio ====");
		ObjetoRespuestaUO data = new ObjetoRespuestaUO();
		ObjetoRespuestaParametroAll dataAll = new ObjetoRespuestaParametroAll();
		ObjetoRespuestaParametroUnit dataUnit = new ObjetoRespuestaParametroUnit();
		RestfulParametroAll respuestaRestAll = new RestfulParametroAll();	
		RestfulParametroUnit respuestaRestUnit = new RestfulParametroUnit();
		
		ObjectMapper mapper = new ObjectMapper();
		String json = StringUtils.EMPTY;
		try {
			json = mapper.writeValueAsString(map);
		} catch (IOException e) {
			Constante.loggerUsuarioApi.error(Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR+"al convertir el json de entrada.");
		}catch(Exception e){
			Constante.loggerUsuarioApi.error(Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR+"al convertir el json de entrada.");
		}

		Constante.loggerUsuarioApi.info("json : " + json);
		
		ParametroBean paramData = new ParametroBean();
		
		int idRpta = 0;		
						
		Constante.loggerUsuarioApi.info("obtenerParametro-entrada: " + json);
		paramData.setTipo(tipo);
		if(tipo.equalsIgnoreCase("all")){
			dataAll = parametroService.listarParametroAll(paramData);
			idRpta = Integer.parseInt(dataAll.getCodigo());
		}else {
			dataUnit = parametroService.listarParametroUnit(paramData);
			idRpta = Integer.parseInt(dataUnit.getCodigo());
		}		
		
		//Verificacion de rptas y setear Mensaje
		if (idRpta > 0) {
			if(tipo.equalsIgnoreCase("all")){
				dataAll.setCodigo(String.valueOf(idRpta));
				dataAll.setValor(Constante.MENSAJE.MSJ_OK);
			}else{
				dataUnit.setCodigo(String.valueOf(idRpta));
				dataUnit.setValor(Constante.MENSAJE.MSJ_OK);
			}			
		} else if (idRpta == -1) {
			if(tipo.equalsIgnoreCase("all")){
				dataAll.setCodigo(Constante.PARAMETRO.COD_RPTA_EXCEPTION);
				dataAll.setValor(Constante.MENSAJE.MSJ_ERROR);
			}else{
				dataUnit.setCodigo(Constante.PARAMETRO.COD_RPTA_EXCEPTION);
				dataUnit.setValor(Constante.MENSAJE.MSJ_ERROR);
			}			
		} else if (idRpta == 0) {
			if(tipo.equalsIgnoreCase("all")){
				dataAll.setCodigo(Constante.PARAMETRO.COD_RPTA_NO_EXITO);
				dataAll.setValor(Constante.MENSAJE.MSJ_NO_DATA);
			}else{
				dataUnit.setCodigo(Constante.PARAMETRO.COD_RPTA_NO_EXITO);
				dataUnit.setValor(Constante.MENSAJE.MSJ_NO_DATA);
			}			
		}
		
		//Set RestFul Response
		if(tipo.equalsIgnoreCase("all")){
			respuestaRestAll.setData(dataAll);
			respuestaRestAll.setMessageError(null);
		}
		else {
			respuestaRestUnit.setData(dataUnit);
			respuestaRestUnit.setMessageError(null);
		}		
		
		
		Constante.loggerUsuarioApi.info("-----------------------------------");
		return new ResponseEntity<Object>(tipo.equalsIgnoreCase("all")?respuestaRestAll:respuestaRestUnit, HttpStatus.CREATED);		

				
	}	
			
	@RequestMapping(value = "/", method = RequestMethod.POST, headers = "Accept=application/json", produces = "text/plain;charset=UTF-8")
	public @ResponseBody ResponseEntity<Object> registrarUO(@RequestBody Map<String, Object> map) {

		Constante.loggerUsuarioApi.debug("=== /registrarUO/ : inicio ====");
		ObjetoRespuestaUO data = new ObjetoRespuestaUO();
		RestfulUO respuestaRest = new RestfulUO();
		MessageError messageError =  new MessageError();
		
		ObjectMapper mapper = new ObjectMapper();
		String json = StringUtils.EMPTY;
		try {
			json = mapper.writeValueAsString(map);
		} catch (IOException e) {
			Constante.loggerUsuarioApi.error(Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR+"al convertir el json de entrada.");
		}catch(Exception e){
			Constante.loggerUsuarioApi.error(Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR+"al convertir el json de entrada.");
		}

		Constante.loggerUsuarioApi.info("json : " + json);

		
		UOSistemaBean uoData = null;
		
		int idRpta = 0;			
							
		//Agregar un Usuario.						
		Constante.loggerUsuarioApi.info("registrarUsuario-entrada: " + json);
		uoData = mapper.convertValue(map.get("data"), UOSistemaBean.class);
		data = uoService.agregarUO(uoData);
		idRpta = Integer.parseInt(data.getCodigo());
					
		
		//Verificacion de rptas y setear Mensaje
		if (idRpta > 0) {
			data.setCodigo(String.valueOf(idRpta));
			data.setValor(Constante.MENSAJE.MSJ_OK);
		} else if (idRpta == -1) {
			data.setCodigo(Constante.PARAMETRO.COD_RPTA_EXCEPTION);
			data.setValor(Constante.MENSAJE.MSJ_ERROR);
		} else if (idRpta == 0) {
			data.setCodigo(Constante.PARAMETRO.COD_RPTA_NO_EXITO);
			data.setValor(Constante.MENSAJE.MSJ_NO_DATA);
		}
		
		//Set RestFul Response
		respuestaRest.setData(data);
		respuestaRest.setMessageError(null);
		Constante.loggerUsuarioApi.info("-----------------------------------");
		return new ResponseEntity<Object>(respuestaRest, HttpStatus.CREATED);			

	
		//data.setCodigo(Constante.PARAMETRO.COD_RPTA_NO_EXITO);
		//data.setValor(Constante.MENSAJE.MSJ_NO_DATA);
		
		//Set Error Message			
		//messageError = Utilitario.sendHttpClientErrors(401);
		
		//Set RestFul Response
		//respuestaRest.setData(data);
		//respuestaRest.setMessageError(messageError);
		
		//Constante.loggerUsuarioApi.error("----- salida-ERROR: "+Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR + "al registrar usuario. ");
		//return new ResponseEntity<Object>(respuestaRest, messageError.getHttpStatus());
				
		
	}
	
	@RequestMapping(value = "/", method = RequestMethod.PUT, headers = "Accept=application/json", produces = "text/plain;charset=UTF-8")
	public @ResponseBody ResponseEntity<Object> actualizarUO(@RequestBody Map<String, Object> map) {

		Constante.loggerUsuarioApi.debug("=== /usuarios/ : inicio ====");
		ObjetoRespuestaUO data = new ObjetoRespuestaUO();
		RestfulUO respuestaRest = new RestfulUO();
		MessageError messageError =  new MessageError();
		
		ObjectMapper mapper = new ObjectMapper();
		String json = StringUtils.EMPTY;
		try {
			json = mapper.writeValueAsString(map);
		} catch (IOException e) {
			Constante.loggerUsuarioApi.error(Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR+"al convertir el json de entrada.");
		}catch(Exception e){
			Constante.loggerUsuarioApi.error(Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR+"al convertir el json de entrada.");
		}

		Constante.loggerUsuarioApi.info("json : " + json);

		
		UOSistemaBean uoData = null;
		
		int idRpta = 0;			
							
		//Actualizar un Usuario.						
		Constante.loggerUsuarioApi.info("actualizarUsuario-entrada: " + json);
		uoData = mapper.convertValue(map.get("data"), UOSistemaBean.class);
		data = uoService.actualizarUO(uoData);
		idRpta = Integer.parseInt(data.getCodigo());
					
		
		//Verificacion de rptas y setear Mensaje
		if (idRpta > 0) {
			data.setCodigo(String.valueOf(idRpta));
			data.setValor(Constante.MENSAJE.MSJ_OK);
		} else if (idRpta == -1) {
			data.setCodigo(Constante.PARAMETRO.COD_RPTA_EXCEPTION);
			data.setValor(Constante.MENSAJE.MSJ_ERROR);
		} else if (idRpta == 0) {
			data.setCodigo(Constante.PARAMETRO.COD_RPTA_NO_EXITO);
			data.setValor(Constante.MENSAJE.MSJ_NO_DATA);
		}
		
		Constante.loggerUsuarioApi.info("-----------------------------------");
		return new ResponseEntity<Object>(data, HttpStatus.CREATED);	
		
	}
	
	@RequestMapping(value = "/", method = RequestMethod.DELETE, headers = "Accept=application/json", produces = "text/plain;charset=UTF-8")
	public @ResponseBody ResponseEntity<Object> eliminarUO(@RequestBody Map<String, Object> map) {

		Constante.loggerUsuarioApi.debug("=== /eliminarUO/ : inicio ====");
		ObjetoRespuestaUO data = new ObjetoRespuestaUO();
		RestfulUO respuestaRest = new RestfulUO();
		MessageError messageError =  new MessageError();
		
		ObjectMapper mapper = new ObjectMapper();
		String json = StringUtils.EMPTY;
		try {
			json = mapper.writeValueAsString(map);
		} catch (IOException e) {
			Constante.loggerUsuarioApi.error(Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR+"al convertir el json de entrada.");
		}catch(Exception e){
			Constante.loggerUsuarioApi.error(Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR+"al convertir el json de entrada.");
		}

		Constante.loggerUsuarioApi.info("json : " + json);

		
		UOSistemaBean uoData = null;
		
		int idRpta = 0;			
							
		//Eliminar un Usuario.						
		Constante.loggerUsuarioApi.info("eliminarUsuario-entrada: " + json);
		uoData = mapper.convertValue(map.get("data"), UOSistemaBean.class);
		data = uoService.eliminarUO(uoData);
		idRpta = Integer.parseInt(data.getCodigo());
		
		//Verificacion de rptas y setear Mensaje
		if (idRpta > 0) {
			data.setCodigo(String.valueOf(idRpta));
			data.setValor(Constante.MENSAJE.MSJ_OK);
		} else if (idRpta == -1) {
			data.setCodigo(Constante.PARAMETRO.COD_RPTA_EXCEPTION);
			data.setValor(Constante.MENSAJE.MSJ_ERROR);
		} else if (idRpta == 0) {
			data.setCodigo(Constante.PARAMETRO.COD_RPTA_NO_EXITO);
			data.setValor(Constante.MENSAJE.MSJ_NO_DATA);
		}
		
		Constante.loggerUsuarioApi.info("-----------------------------------");
		return new ResponseEntity<Object>(data, HttpStatus.CREATED);					
		
	}
		
	
	//Validacion de usuario generico para metodo POST
	private int verificarDatoUsuario(ObjectMapper mapper, Map<String, Object> map){		
		UsuarioRestBean usuarioWsBean = mapper.convertValue(map.get("security"), UsuarioRestBean.class);
		byte[] decodedUsuario = Base64.decodeBase64(usuarioWsBean.getUsuario());
		String usuario = new String(decodedUsuario);
		byte[] decodedPassword = Base64.decodeBase64(usuarioWsBean.getPassword());
		String password = new String(decodedPassword);
		
		int resp = seguridadService.autenticarUsuario(usuario, password);
		return resp;
	}	

}
