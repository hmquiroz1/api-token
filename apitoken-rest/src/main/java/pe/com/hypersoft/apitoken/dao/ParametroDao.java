package pe.com.hypersoft.apitoken.dao;

import java.util.List;

import pe.com.hypersoft.apitoken.bean.ParametroBean;
import pe.com.hypersoft.apitoken.bean.UOSistemaBean;
import pe.com.hypersoft.apitoken.bean.UsuarioSistemaBean;

public interface ParametroDao {

	int agregarUO(UOSistemaBean uoSistemaBean);
	List<UOSistemaBean> obtenerUO(UOSistemaBean uoSistemaBean);
	int actualizarUO(UOSistemaBean uoSistemaBean);
	int eliminarUO(UOSistemaBean uoSistemaBean);
	List<ParametroBean> listarParametroAll(ParametroBean parametroBean);
	List<ParametroBean> listarParametroUnit(ParametroBean parametroBean);
}
