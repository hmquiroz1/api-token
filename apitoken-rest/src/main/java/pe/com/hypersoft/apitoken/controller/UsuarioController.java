package pe.com.hypersoft.apitoken.controller;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;

import pe.com.hypersoft.apitoken.bean.MessageError;
import pe.com.hypersoft.apitoken.bean.ObjetoRespuestaMenuOpciones;
import pe.com.hypersoft.apitoken.bean.ObjetoRespuestaUsuario;
import pe.com.hypersoft.apitoken.bean.UsuarioRestBean;
import pe.com.hypersoft.apitoken.bean.UsuarioSistemaBean;
import pe.com.hypersoft.apitoken.restful.RestfulUsuario;
import pe.com.hypersoft.apitoken.service.JwtService;
import pe.com.hypersoft.apitoken.service.SeguridadService;
import pe.com.hypersoft.apitoken.util.Constante;
import pe.com.hypersoft.apitoken.util.Utilitario;

@Controller
@RequestMapping(value = "/usuarios")
public class UsuarioController {
	
	@Autowired
	SeguridadService seguridadService;
	
	@Autowired
	JwtService jwtService;
	
	@RequestMapping(value = "/{email}/", method = RequestMethod.GET, headers = "Accept=application/json", produces = "text/plain;charset=UTF-8")
	public @ResponseBody ResponseEntity<Object> obtenerUsuario(@PathVariable("email") String email,@RequestBody Map<String, Object> map) {

		Constante.loggerUsuarioApi.debug("=== /api-token/rest : inicio ====");
		ObjetoRespuestaUsuario data = new ObjetoRespuestaUsuario();
		RestfulUsuario respuestaRest = new RestfulUsuario();
		MessageError messageError =  new MessageError();
		
		ObjectMapper mapper = new ObjectMapper();
		String json = StringUtils.EMPTY;
		try {
			json = mapper.writeValueAsString(map);
		} catch (IOException e) {
			Constante.loggerUsuarioApi.error(Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR+"al convertir el json de entrada.");
		}catch(Exception e){
			Constante.loggerUsuarioApi.error(Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR+"al convertir el json de entrada.");
		}

		Constante.loggerUsuarioApi.info("json : " + json);

		int resp = verificarDatoUsuario(mapper, map);

		if (resp == 1) {

			UsuarioSistemaBean usuarioData = new UsuarioSistemaBean();
			
			int idRpta = 0;		
							
			Constante.loggerUsuarioApi.info("consultarUsuario-entrada: " + json);
			usuarioData.setEmail(email);
			data = seguridadService.obtenerUsuario(usuarioData);
			idRpta = Integer.parseInt(data.getCodigo());
			
			
			//Verificacion de rptas y setear Mensaje
			if (idRpta > 0) {
				data.setCodigo(String.valueOf(idRpta));
				data.setValor(Constante.MENSAJE.MSJ_OK);
			} else if (idRpta == -1) {
				data.setCodigo(Constante.PARAMETRO.COD_RPTA_EXCEPTION);
				data.setValor(Constante.MENSAJE.MSJ_ERROR);
			} else if (idRpta == 0) {
				data.setCodigo(Constante.PARAMETRO.COD_RPTA_NO_EXITO);
				data.setValor(Constante.MENSAJE.MSJ_NO_DATA);
			}
			
			//Set RestFul Response
			respuestaRest.setData(data);
			respuestaRest.setMessageError(null);
			
			Constante.loggerUsuarioApi.info("-----------------------------------");
			return new ResponseEntity<Object>(respuestaRest, HttpStatus.CREATED);			

		} else {
			data.setCodigo(Constante.PARAMETRO.COD_RPTA_NO_EXITO);
			data.setValor(Constante.MENSAJE.MSJ_NO_DATA);
			
			//Set Error Message			
			messageError = Utilitario.sendHttpClientErrors(401);
			
			//Set RestFul Response
			respuestaRest.setData(data);
			respuestaRest.setMessageError(messageError);
			
			Constante.loggerUsuarioApi.error("----- salida-ERROR: "+Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR + "al verificarDatoUsuario. ");
			return new ResponseEntity<Object>(data, messageError.getHttpStatus());
		}		
	}	
		
	@RequestMapping(value = "/", method = RequestMethod.POST, headers = "Accept=application/json", produces = "text/plain;charset=UTF-8")
	public @ResponseBody ResponseEntity<Object> registrarUsuario(@RequestBody Map<String, Object> map) {

		Constante.loggerUsuarioApi.debug("=== /usuarios/ : inicio ====");
		ObjetoRespuestaUsuario data = new ObjetoRespuestaUsuario();
		RestfulUsuario respuestaRest = new RestfulUsuario();
		MessageError messageError =  new MessageError();
		
		ObjectMapper mapper = new ObjectMapper();
		String json = StringUtils.EMPTY;
		try {
			json = mapper.writeValueAsString(map);
		} catch (IOException e) {
			Constante.loggerUsuarioApi.error(Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR+"al convertir el json de entrada.");
		}catch(Exception e){
			Constante.loggerUsuarioApi.error(Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR+"al convertir el json de entrada.");
		}

		Constante.loggerUsuarioApi.info("json : " + json);

		int resp = verificarDatoUsuario(mapper, map);

		if (resp == 1) {

			UsuarioSistemaBean usuarioData = null;
			
			int idRpta = 0;			
								
			//Agregar un Usuario.						
			Constante.loggerUsuarioApi.info("registrarUsuario-entrada: " + json);
			usuarioData = mapper.convertValue(map.get("objeto"), UsuarioSistemaBean.class);
			data = seguridadService.agregarUsuario(usuarioData);
			idRpta = Integer.parseInt(data.getCodigo());
						
			
			//Verificacion de rptas y setear Mensaje
			if (idRpta > 0) {
				data.setCodigo(String.valueOf(idRpta));
				data.setValor(Constante.MENSAJE.MSJ_OK);
			} else if (idRpta == -1) {
				data.setCodigo(Constante.PARAMETRO.COD_RPTA_EXCEPTION);
				data.setValor(Constante.MENSAJE.MSJ_ERROR);
			} else if (idRpta == 0) {
				data.setCodigo(Constante.PARAMETRO.COD_RPTA_NO_EXITO);
				data.setValor(Constante.MENSAJE.MSJ_NO_DATA);
			}
			
			//Set RestFul Response
			respuestaRest.setData(data);
			respuestaRest.setMessageError(null);
			Constante.loggerUsuarioApi.info("-----------------------------------");
			return new ResponseEntity<Object>(respuestaRest, HttpStatus.CREATED);			

		} else {
			data.setCodigo(Constante.PARAMETRO.COD_RPTA_NO_EXITO);
			data.setValor(Constante.MENSAJE.MSJ_NO_DATA);
			
			//Set Error Message			
			messageError = Utilitario.sendHttpClientErrors(401);
			
			//Set RestFul Response
			respuestaRest.setData(data);
			respuestaRest.setMessageError(messageError);
			
			Constante.loggerUsuarioApi.error("----- salida-ERROR: "+Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR + "al registrar usuario. ");
			return new ResponseEntity<Object>(respuestaRest, messageError.getHttpStatus());
		}			
		
	}
	
	@RequestMapping(value = "/", method = RequestMethod.PUT, headers = "Accept=application/json", produces = "text/plain;charset=UTF-8")
	public @ResponseBody ResponseEntity<Object> actualizarUsuario(@RequestBody Map<String, Object> map) {

		Constante.loggerUsuarioApi.debug("=== /usuarios/ : inicio ====");
		ObjetoRespuestaUsuario data = new ObjetoRespuestaUsuario();
		RestfulUsuario respuestaRest = new RestfulUsuario();
		MessageError messageError =  new MessageError();
		
		ObjectMapper mapper = new ObjectMapper();
		String json = StringUtils.EMPTY;
		try {
			json = mapper.writeValueAsString(map);
		} catch (IOException e) {
			Constante.loggerUsuarioApi.error(Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR+"al convertir el json de entrada.");
		}catch(Exception e){
			Constante.loggerUsuarioApi.error(Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR+"al convertir el json de entrada.");
		}

		Constante.loggerUsuarioApi.info("json : " + json);

		int resp = verificarDatoUsuario(mapper, map);

		if (resp == 1) {

			UsuarioSistemaBean usuarioData = null;
			
			int idRpta = 0;			
								
			//Actualizar un Usuario.						
			Constante.loggerUsuarioApi.info("actualizarUsuario-entrada: " + json);
			usuarioData = mapper.convertValue(map.get("objeto"), UsuarioSistemaBean.class);
			data = seguridadService.actualizarUsuario(usuarioData);
			idRpta = Integer.parseInt(data.getCodigo());
						
			
			//Verificacion de rptas y setear Mensaje
			if (idRpta > 0) {
				data.setCodigo(String.valueOf(idRpta));
				data.setValor(Constante.MENSAJE.MSJ_OK);
			} else if (idRpta == -1) {
				data.setCodigo(Constante.PARAMETRO.COD_RPTA_EXCEPTION);
				data.setValor(Constante.MENSAJE.MSJ_ERROR);
			} else if (idRpta == 0) {
				data.setCodigo(Constante.PARAMETRO.COD_RPTA_NO_EXITO);
				data.setValor(Constante.MENSAJE.MSJ_NO_DATA);
			}
			
			Constante.loggerUsuarioApi.info("-----------------------------------");
			return new ResponseEntity<Object>(data, HttpStatus.CREATED);			

		} else {
			data.setCodigo(Constante.PARAMETRO.COD_RPTA_NO_EXITO);
			data.setValor(Constante.MENSAJE.MSJ_NO_DATA);
			
			//Set Error Message			
			messageError = Utilitario.sendHttpClientErrors(401);
			
			//Set RestFul Response
			respuestaRest.setData(data);
			respuestaRest.setMessageError(messageError);
			
			Constante.loggerUsuarioApi.error("----- salida-ERROR: "+Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR + "al registrar usuario. ");
			return new ResponseEntity<Object>(data, messageError.getHttpStatus());
		}			
		
	}
	
	@RequestMapping(value = "/", method = RequestMethod.DELETE, headers = "Accept=application/json", produces = "text/plain;charset=UTF-8")
	public @ResponseBody ResponseEntity<Object> eliminarUsuario(@RequestBody Map<String, Object> map) {

		Constante.loggerUsuarioApi.debug("=== /usuarios/ : inicio ====");
		ObjetoRespuestaUsuario data = new ObjetoRespuestaUsuario();
		RestfulUsuario respuestaRest = new RestfulUsuario();
		MessageError messageError =  new MessageError();
		
		ObjectMapper mapper = new ObjectMapper();
		String json = StringUtils.EMPTY;
		try {
			json = mapper.writeValueAsString(map);
		} catch (IOException e) {
			Constante.loggerUsuarioApi.error(Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR+"al convertir el json de entrada.");
		}catch(Exception e){
			Constante.loggerUsuarioApi.error(Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR+"al convertir el json de entrada.");
		}

		Constante.loggerUsuarioApi.info("json : " + json);

		int resp = verificarDatoUsuario(mapper, map);

		if (resp == 1) {

			UsuarioSistemaBean usuarioData = null;
			
			int idRpta = 0;			
								
			//Eliminar un Usuario.						
			Constante.loggerUsuarioApi.info("eliminarUsuario-entrada: " + json);
			usuarioData = mapper.convertValue(map.get("objeto"), UsuarioSistemaBean.class);
			data = seguridadService.eliminarUsuario(usuarioData);
			idRpta = Integer.parseInt(data.getCodigo());
			
			//Verificacion de rptas y setear Mensaje
			if (idRpta > 0) {
				data.setCodigo(String.valueOf(idRpta));
				data.setValor(Constante.MENSAJE.MSJ_OK);
			} else if (idRpta == -1) {
				data.setCodigo(Constante.PARAMETRO.COD_RPTA_EXCEPTION);
				data.setValor(Constante.MENSAJE.MSJ_ERROR);
			} else if (idRpta == 0) {
				data.setCodigo(Constante.PARAMETRO.COD_RPTA_NO_EXITO);
				data.setValor(Constante.MENSAJE.MSJ_NO_DATA);
			}
			
			Constante.loggerUsuarioApi.info("-----------------------------------");
			return new ResponseEntity<Object>(data, HttpStatus.CREATED);			

		} else {
			data.setCodigo(Constante.PARAMETRO.COD_RPTA_NO_EXITO);
			data.setValor(Constante.MENSAJE.MSJ_NO_DATA);
			
			//Set Error Message			
			messageError = Utilitario.sendHttpClientErrors(401);
			
			//Set RestFul Response
			respuestaRest.setData(data);
			respuestaRest.setMessageError(messageError);
			
			Constante.loggerUsuarioApi.error("----- salida-ERROR: "+Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR + "al registrar usuario. ");
			return new ResponseEntity<Object>(data, messageError.getHttpStatus());
		}			
		
	}
	
	@RequestMapping(value = "/listarmenu", method = RequestMethod.POST, headers = "Accept=application/json", produces = "text/plain;charset=UTF-8")
	public @ResponseBody ResponseEntity<Object> listarMenu(HttpServletRequest request,@RequestBody Map<String, Object> map) {
	
		Constante.loggerUsuarioApi.debug("=== /listarMenu/ : inicio ====");
		
		HttpHeaders headers = new HttpHeaders();
		//Set Headers
		headers.add("Content-Type", "application/json; charset=UTF-8");	
		
		ObjetoRespuestaMenuOpciones data = new ObjetoRespuestaMenuOpciones();
		ObjectMapper mapper = new ObjectMapper();
		
		String json = StringUtils.EMPTY;
		try {
			json = mapper.writeValueAsString(map);
		} catch (IOException e) {
			Constante.loggerUsuarioApi.error(Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR+"al convertir el json de entrada.");
		}catch(Exception e){
			Constante.loggerUsuarioApi.error(Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR+"al convertir el json de entrada.");
		}
		
		UsuarioSistemaBean usuarioData = null;
		int idRpta = 0;
			
		Constante.loggerUsuarioApi.info("usuarios-POST: " + json);
		usuarioData = mapper.convertValue(map.get("autenticacion"), UsuarioSistemaBean.class);
		
		if (usuarioData.getPerfil() == null) {
			headers.add("Refusal-Motivation", "datos incompletos");
			data.setCodigo(Constante.PARAMETRO.COD_RPTA_EXCEPTION);
			data.setValor(Constante.MENSAJE.MSJ_ERROR);
			return new ResponseEntity<Object>(data,headers, HttpStatus.UNAUTHORIZED);
        }
		
		data = seguridadService.listarMenuOpciones(usuarioData);
		idRpta = Integer.parseInt(data.getCodigo());
	
		//Verificacion de rptas y setear Mensaje
		if (idRpta > 0) {
			data.setCodigo(String.valueOf(idRpta));
			data.setValor(Constante.MENSAJE.MSJ_OK);			
		} else if (idRpta == -1) {
			headers.add("Refusal-Motivation", "credenciales invalidas");
			headers.add("token", null);
			data.setCodigo(Constante.PARAMETRO.COD_RPTA_EXCEPTION);
			data.setValor(Constante.MENSAJE.MSJ_ERROR);			
		} else if (idRpta == 0) {
			headers.add("Refusal-Motivation", "no existe data");
			headers.add("token", null);
			data.setCodigo(Constante.PARAMETRO.COD_RPTA_NO_EXITO);
			data.setValor(Constante.MENSAJE.MSJ_NO_DATA);			
		}	
		
		Constante.loggerUsuarioApi.debug("=== /listarMenu/ : fin ====");							
		return new ResponseEntity<Object>(data,headers, HttpStatus.OK);
	}
	
	/*@RequestMapping(value = "/", method = RequestMethod.POST, headers = "Accept=application/json", produces = "text/plain;charset=UTF-8")
	public @ResponseBody ResponseEntity<Object> registrarSolicitud(@RequestBody Map<String, Object> map) {

		Constante.loggerUsuarioApi.debug("=== /api-token/rest : inicio ====");
		ObjetoRespuestaUsuario data = new ObjetoRespuestaUsuario();
		ObjectMapper mapper = new ObjectMapper();
		String json = StringUtils.EMPTY;
		try {
			json = mapper.writeValueAsString(map);
		} catch (IOException e) {
			Constante.loggerUsuarioApi.error(Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR+"al convertir el json de entrada.");
		}catch(Exception e){
			Constante.loggerUsuarioApi.error(Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR+"al convertir el json de entrada.");
		}

		Constante.loggerUsuarioApi.info("json : " + json);

		int resp = verificarDatoUsuario(mapper, map);

		if (resp == 1) {

			UsuarioSistemaBean usuarioData = null;
			
			int idRpta = 0;
			
			if (map.get("tipoServicio") != null) {
				String tipo = (String) map.get("tipoServicio");				
				
				
				//Consulta de un Usuario.
				if (tipo.equalsIgnoreCase(Constante.TIPO_OPERACION.CONSULTA_USUARIO)) {					
					Constante.loggerUsuarioApi.info("consultarUsuario-entrada: " + json);
					usuarioData = mapper.convertValue(map.get("objeto"), UsuarioSistemaBean.class);
					data = seguridadService.obtenerUsuario(usuarioData);
					idRpta = Integer.parseInt(data.getCodigo());
				}
				
				//Agregar un Usuario.
				if (tipo.equalsIgnoreCase(Constante.TIPO_OPERACION.AGREGAR_USUARIO)) {					
					Constante.loggerUsuarioApi.info("agregarUsuario-entrada: " + json);
					usuarioData = mapper.convertValue(map.get("objeto"), UsuarioSistemaBean.class);
					data = seguridadService.agregarUsuario(usuarioData);
					idRpta = Integer.parseInt(data.getCodigo());
				}
				
				//Actualizar un Usuario
				if (tipo.equalsIgnoreCase(Constante.TIPO_OPERACION.ACTUALIZAR_USUARIO)) {					
					Constante.loggerUsuarioApi.info("actualizarUsuario-entrada: " + json);
					usuarioData = mapper.convertValue(map.get("objeto"), UsuarioSistemaBean.class);
					data = seguridadService.actualizarUsuario(usuarioData);
					idRpta = Integer.parseInt(data.getCodigo());
				}
				
				//Eliminar un Usuario
				if (tipo.equalsIgnoreCase(Constante.TIPO_OPERACION.ELIMINAR_USUARIO)) {					
					Constante.loggerUsuarioApi.info("eliminarUsuario-entrada: " + json);
					usuarioData = mapper.convertValue(map.get("objeto"), UsuarioSistemaBean.class);
					data = seguridadService.eliminarUsuario(usuarioData);
					idRpta = Integer.parseInt(data.getCodigo());
				}
				
				//Consulta de un Usuario.
				if (tipo.equalsIgnoreCase(Constante.TIPO_OPERACION.LST_MENU_OPCIONES)) {					
					Constante.loggerUsuarioApi.info("listarMenuOpciones-entrada: " + json);
					usuarioData = mapper.convertValue(map.get("objeto"), UsuarioSistemaBean.class);
					data = seguridadService.listarMenuOpciones(usuarioData);
					if(data.getCodigo().equalsIgnoreCase("1")){//OK
						data.setListaMenuOpciones(Utilitario.reordenarPadreHijoMenuOpciones(data.getListaMenuOpciones()));
					}
					idRpta = Integer.parseInt(data.getCodigo());
				}
				
				//Verificacion de rptas y setear Mensaje
				if (idRpta > 0) {
					data.setCodigo(String.valueOf(idRpta));
					data.setValor(Constante.MENSAJE.MSJ_OK);
				} else if (idRpta == -1) {
					data.setCodigo(Constante.PARAMETRO.COD_RPTA_EXCEPTION);
					data.setValor(Constante.MENSAJE.MSJ_ERROR);
				} else if (idRpta == 0) {
					data.setCodigo(Constante.PARAMETRO.COD_RPTA_NO_EXITO);
					data.setValor(Constante.MENSAJE.MSJ_NO_DATA);
				}
				
				Constante.loggerUsuarioApi.info("-----------------------------------");
				return new ResponseEntity<Object>(data, HttpStatus.CREATED);
			}

		} else {
			data.setCodigo(Constante.PARAMETRO.COD_RPTA_NO_EXITO);
			data.setValor(Constante.MENSAJE.MSJ_NO_DATA);
			Constante.loggerUsuarioApi.error("----- salida-ERROR: "+Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR + "al verificarDatoUsuario. ");
			return new ResponseEntity<Object>(data, HttpStatus.CREATED);
		}
		
		Constante.loggerUsuarioApi.debug("=== /api-token/rest : fin ====");

		return new ResponseEntity<Object>(HttpStatus.BAD_REQUEST);
	}*/
	
	//Validacion de usuario generico para metodo POST
	private int verificarDatoUsuario(ObjectMapper mapper, Map<String, Object> map){		
		UsuarioRestBean usuarioWsBean = mapper.convertValue(map.get("security"), UsuarioRestBean.class);
		byte[] decodedUsuario = Base64.decodeBase64(usuarioWsBean.getUsuario());
		String usuario = new String(decodedUsuario);
		byte[] decodedPassword = Base64.decodeBase64(usuarioWsBean.getPassword());
		String password = new String(decodedPassword);
		
		int resp = seguridadService.autenticarUsuario(usuario, password);
		return resp;
	}	

}
