package pe.com.hypersoft.apitoken.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import pe.com.hypersoft.apitoken.bean.AgenciaBean;
import pe.com.hypersoft.apitoken.bean.OcurrenciaBean;
import pe.com.hypersoft.apitoken.bean.UOSistemaBean;
import pe.com.hypersoft.apitoken.bean.UsuarioRestBean;
import pe.com.hypersoft.apitoken.bean.UsuarioSistemaBean;

@Component
@SuppressWarnings("rawtypes")
public class AgenciaRowMapper implements RowMapper {

	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		AgenciaBean ag = new AgenciaBean();
		ag.setDistrito(rs.getString("DISTRITO"));
		ag.setProvincia(rs.getString("PROVINCIA"));
		ag.setDepartamento(rs.getString("DEPARTAMENTO"));
		ag.setDireccion(rs.getString("DIRECCION"));
		ag.setLat(rs.getString("LAT"));
		ag.setLon(rs.getString("LON"));
				
		return ag;
	}
}