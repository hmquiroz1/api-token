package pe.com.hypersoft.apitoken.bean;

import java.io.Serializable;
import java.util.Date;


@SuppressWarnings("serial")
public class UOSistemaBean implements Serializable{

	private Long id;	
	private String descripcion;
	private String nombre;	
	private String fecinivig;
	private String fecfinvig;	
	private String estado;	
	
	public UOSistemaBean(){
		
	}
			
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}


	public String getDescripcion() {
		return descripcion;
	}


	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getFecinivig() {
		return fecinivig;
	}


	public void setFecinivig(String fecinivig) {
		this.fecinivig = fecinivig;
	}


	public String getFecfinvig() {
		return fecfinvig;
	}


	public void setFecfinvig(String fecfinvig) {
		this.fecfinvig = fecfinvig;
	}


	public String getEstado() {
		return estado;
	}


	public void setEstado(String estado) {
		this.estado = estado;
	}		
	
}
