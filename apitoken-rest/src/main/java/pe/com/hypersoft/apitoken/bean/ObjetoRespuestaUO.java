package pe.com.hypersoft.apitoken.bean;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
public class ObjetoRespuestaUO implements Serializable{
	
	private String codigo;
	private String valor;
	private List<UOSistemaBean> listaUO;
	
	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}
	
	public String getValor() {
		return valor;
	}

	public List<UOSistemaBean> getListaUO() {
		return listaUO;
	}

	public void setListaUO(List<UOSistemaBean> listaUO) {
		this.listaUO = listaUO;
	}
		
}