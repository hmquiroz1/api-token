package pe.com.hypersoft.apitoken.controller;

import java.io.IOException;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import pe.com.hypersoft.apitoken.bean.AgenciaBean;
import pe.com.hypersoft.apitoken.bean.MessageError;
import pe.com.hypersoft.apitoken.bean.ObjetoRespuestaAgencia;
import pe.com.hypersoft.apitoken.bean.OcurrenciaBean;
import pe.com.hypersoft.apitoken.bean.UsuarioRestBean;
import pe.com.hypersoft.apitoken.restful.RestfulAgencia;
import pe.com.hypersoft.apitoken.service.AgenciaService;
import pe.com.hypersoft.apitoken.service.JwtService;
import pe.com.hypersoft.apitoken.service.SeguridadService;
import pe.com.hypersoft.apitoken.util.Constante;
import pe.com.hypersoft.apitoken.util.Utilitario;

@RestController
@Controller
@RequestMapping(value = "/agencias")
@Api(value="ocurrenciaController", description="Operaciones para Mantenimiento de Agencias")
public class AgenciasController {
	
	@Autowired
	SeguridadService seguridadService;
	
	@Autowired
	AgenciaService agenciaService;
	
	@Autowired
	JwtService jwtService;
	
	@ApiOperation(value = "Obtener Lista de Agencias", notes = "Retorna listado ocurrencias por fechas",response = Object.class)
	@ApiResponses(value = {
	        @ApiResponse(code = 200, message = "Agencias obtenidas satisfactoriamente"),
	        @ApiResponse(code = 401, message = "La autentificacion ha fallado o no ha sido provista")
	}
	)
	@RequestMapping(value = "/{name}", method = RequestMethod.GET, headers = "Accept=application/json", produces = "text/plain;charset=UTF-8")
	public @ResponseBody ResponseEntity<Object> obtenerOcurrencias(
						@ApiParam("Nombre Agencia es obligatorio.")@PathVariable("name") String nombreAgencia,
						@ApiParam("Map debe contener el objeto security.")@RequestBody Map<String, Object> map) {

		Constante.loggerUsuarioApi.debug("=== /api-token/rest : inicio ====");
		ObjetoRespuestaAgencia data = new ObjetoRespuestaAgencia();
		RestfulAgencia respuestaRest = new RestfulAgencia();
		MessageError messageError =  new MessageError();
		
		ObjectMapper mapper = new ObjectMapper();
		String json = StringUtils.EMPTY;
		try {
			json = mapper.writeValueAsString(map);
		} catch (IOException e) {
			Constante.loggerUsuarioApi.error(Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR+"al convertir el json de entrada.");
		}catch(Exception e){
			Constante.loggerUsuarioApi.error(Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR+"al convertir el json de entrada.");
		}

		Constante.loggerUsuarioApi.info("json : " + json);

		int resp = verificarDatoUsuario(mapper, map);

		if (resp == 1) {

			AgenciaBean agData = new AgenciaBean();
			
			int idRpta = 0;		
							
			Constante.loggerUsuarioApi.info("obtenerAgencias-entrada: " + json);
			agData.setAgencia(nombreAgencia);
			data = agenciaService.listarAgencia(agData);
			idRpta = Integer.parseInt(data.getCodigo());
			
			
			//Verificacion de rptas y setear Mensaje
			if (idRpta > 0) {
				data.setCodigo(String.valueOf(idRpta));
				data.setValor(Constante.MENSAJE.MSJ_OK);
			} else if (idRpta == -1) {
				data.setCodigo(Constante.PARAMETRO.COD_RPTA_EXCEPTION);
				data.setValor(Constante.MENSAJE.MSJ_ERROR);
			} else if (idRpta == 0) {
				data.setCodigo(Constante.PARAMETRO.COD_RPTA_NO_EXITO);
				data.setValor(Constante.MENSAJE.MSJ_NO_DATA);
			}
			
			//Set RestFul Response
			respuestaRest.setData(data);
			respuestaRest.setMessageError(null);
			
			Constante.loggerUsuarioApi.info("-----------------------------------");
			return new ResponseEntity<Object>(respuestaRest, HttpStatus.CREATED);			

		} else {
			data.setCodigo(Constante.PARAMETRO.COD_RPTA_NO_EXITO);
			data.setValor(Constante.MENSAJE.MSJ_NO_DATA);
			
			//Set Error Message			
			messageError = Utilitario.sendHttpClientErrors(401);
			
			//Set RestFul Response
			respuestaRest.setData(data);
			respuestaRest.setMessageError(messageError);
			
			Constante.loggerUsuarioApi.error("----- salida-ERROR: "+Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR + "al verificarDatoUsuario. ");
			return new ResponseEntity<Object>(data, messageError.getHttpStatus());
		}		
	}	
		
	@ApiOperation(value = "Obtener Lista de Agencias", notes = "Retorna listado ocurrencias por fechas",response = Object.class)
	@ApiResponses(value = {
	        @ApiResponse(code = 200, message = "Ocurrencias obtenidas satisfactoriamente"),
	        @ApiResponse(code = 401, message = "La autentificacion ha fallado o no ha sido provista")
	}
	)
	@RequestMapping(value = "/", method = RequestMethod.POST, headers = "Accept=application/json", produces = "text/plain;charset=UTF-8")
	public @ResponseBody ResponseEntity<Object> obtenerOcurrenciasPost(@RequestBody Map<String, Object> map) {

		Constante.loggerUsuarioApi.debug("=== /api-token/rest : inicio ====");
		ObjetoRespuestaAgencia data = new ObjetoRespuestaAgencia();
		RestfulAgencia respuestaRest = new RestfulAgencia();
		MessageError messageError =  new MessageError();
		
		ObjectMapper mapper = new ObjectMapper();
		String json = StringUtils.EMPTY;
		try {
			json = mapper.writeValueAsString(map);
		} catch (IOException e) {
			Constante.loggerUsuarioApi.error(Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR+"al convertir el json de entrada.");
		}catch(Exception e){
			Constante.loggerUsuarioApi.error(Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR+"al convertir el json de entrada.");
		}

		Constante.loggerUsuarioApi.info("json : " + json);

		int resp = verificarDatoUsuario(mapper, map);

		if (resp == 1) {

			AgenciaBean agData = new AgenciaBean();
			
			int idRpta = 0;		
							
			Constante.loggerUsuarioApi.info("obtenerAgencias-entrada: " + json);
			agData = mapper.convertValue(map.get("objeto"), AgenciaBean.class);
			data = agenciaService.listarAgencia(agData);
			idRpta = Integer.parseInt(data.getCodigo());
			
			
			//Verificacion de rptas y setear Mensaje
			if (idRpta > 0) {
				data.setCodigo(String.valueOf(idRpta));
				data.setValor(Constante.MENSAJE.MSJ_OK);
			} else if (idRpta == -1) {
				data.setCodigo(Constante.PARAMETRO.COD_RPTA_EXCEPTION);
				data.setValor(Constante.MENSAJE.MSJ_ERROR);
			} else if (idRpta == 0) {
				data.setCodigo(Constante.PARAMETRO.COD_RPTA_NO_EXITO);
				data.setValor(Constante.MENSAJE.MSJ_NO_DATA);
			}
			
			//Set RestFul Response
			respuestaRest.setData(data);
			respuestaRest.setMessageError(null);
			
			Constante.loggerUsuarioApi.info("-----------------------------------");
			return new ResponseEntity<Object>(respuestaRest, HttpStatus.CREATED);			

		} else {
			data.setCodigo(Constante.PARAMETRO.COD_RPTA_NO_EXITO);
			data.setValor(Constante.MENSAJE.MSJ_NO_DATA);
			
			//Set Error Message			
			messageError = Utilitario.sendHttpClientErrors(401);
			
			//Set RestFul Response
			respuestaRest.setData(data);
			respuestaRest.setMessageError(messageError);
			
			Constante.loggerUsuarioApi.error("----- salida-ERROR: "+Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR + "al verificarDatoUsuario. ");
			return new ResponseEntity<Object>(data, messageError.getHttpStatus());
		}		
	}
	
	@RequestMapping(value = "/new", method = RequestMethod.POST, headers = "Accept=application/json", produces = "text/plain;charset=UTF-8")
	public @ResponseBody ResponseEntity<Object> registrarAgencia(@RequestBody Map<String, Object> map) {

		Constante.loggerUsuarioApi.debug("=== /api-token/rest : inicio ====");
		String data = "";
		RestfulAgencia respuestaRest = new RestfulAgencia();
		MessageError messageError =  new MessageError();
		
		ObjectMapper mapper = new ObjectMapper();
		String json = StringUtils.EMPTY;
		try {
			json = mapper.writeValueAsString(map);
		} catch (IOException e) {
			Constante.loggerUsuarioApi.error(Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR+"al convertir el json de entrada.");
		}catch(Exception e){
			Constante.loggerUsuarioApi.error(Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR+"al convertir el json de entrada.");
		}

		Constante.loggerUsuarioApi.info("json : " + json);

		int resp = verificarDatoUsuario(mapper, map);

		if (resp == 1) {

			AgenciaBean agData = new AgenciaBean();
			
			int idRpta = 0;		
							
			Constante.loggerUsuarioApi.info("obtenerAgencias-entrada: " + json);
			agData = mapper.convertValue(map.get("objeto"), AgenciaBean.class);
			data = agenciaService.registrarAgencia(agData);
			idRpta = 1;
			
			
			
			respuestaRest.setMessageError(null);
			
			Constante.loggerUsuarioApi.info("-----------------------------------");
			return new ResponseEntity<Object>(respuestaRest, HttpStatus.CREATED);			

		} else {
			
			
			//Set Error Message			
			messageError = Utilitario.sendHttpClientErrors(401);
			
			//Set RestFul Response
						respuestaRest.setMessageError(messageError);
			
			Constante.loggerUsuarioApi.error("----- salida-ERROR: "+Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR + "al verificarDatoUsuario. ");
			return new ResponseEntity<Object>(data, messageError.getHttpStatus());
		}		
	}
	
	//Validacion de usuario generico para metodo POST
	private int verificarDatoUsuario(ObjectMapper mapper, Map<String, Object> map){		
		UsuarioRestBean usuarioWsBean = mapper.convertValue(map.get("security"), UsuarioRestBean.class);
		byte[] decodedUsuario = Base64.decodeBase64(usuarioWsBean.getUsuario());
		String usuario = new String(decodedUsuario);
		byte[] decodedPassword = Base64.decodeBase64(usuarioWsBean.getPassword());
		String password = new String(decodedPassword);
		
		int resp = seguridadService.autenticarUsuario(usuario, password);
		return resp;
	}	

}
