package pe.com.hypersoft.apitoken.util;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;

import pe.com.hypersoft.apitoken.util.crypto.AESUtil;
import pe.com.hypersoft.apitoken.bean.MessageError;
import pe.com.hypersoft.apitoken.bean.PerfilOpcion;


public class Utilitario {

	private static Logger logger = LogManager.getLogger(Utilitario.class);
	
	public static MessageError sendHttpClientErrors(int errorCode){
		MessageError messageResp = new MessageError();
						
		switch (errorCode) {
	        case 400://400 Bad Request  
	        	 	 messageResp.setVersion("1");
			         messageResp.setSeverity("ERROR");
			         messageResp.setHttp_status("400");
			         messageResp.setError_code("badRequest");
			         messageResp.setError_message("La solicitud contiene sintaxis errónea y no debería repetirse");
			         messageResp.setSystem_error_code("serviceUnavailable");
			         messageResp.setSystem_error_description("La solicitud contiene sintaxis errónea y no debería repetirse");
					 break;
	        case 401:  //400 Bad Request  
		   	 	 	 messageResp.setVersion("1");
			         messageResp.setSeverity("ERROR");
			         messageResp.setHttp_status("401");
			         messageResp.setError_code("Unauthorized");
			         messageResp.setError_message("La autentificacion ha fallado o no ha sido provista");
			         messageResp.setSystem_error_code("Unauthorized");
			         messageResp.setSystem_error_description("La autentificacion ha fallado o no ha sido provista");
			         messageResp.setHttpStatus(HttpStatus.UNAUTHORIZED);
					 break;
	        case 402:    //400 Bad Request  
		  	 	 	 messageResp.setVersion("1");
			         messageResp.setSeverity("ERROR");
			         messageResp.setHttp_status("400");
			         messageResp.setError_code("badRequest");
			         messageResp.setError_message("La solicitud contiene sintaxis errónea y no debería repetirse");
			         messageResp.setSystem_error_code("serviceUnavailable");
			         messageResp.setSystem_error_description("La solicitud contiene sintaxis errónea y no debería repetirse");
					 break;
	        case 403:    //400 Bad Request  
		  	 	 	 messageResp.setVersion("1");
			         messageResp.setSeverity("ERROR");
			         messageResp.setHttp_status("400");
			         messageResp.setError_code("badRequest");
			         messageResp.setError_message("La solicitud contiene sintaxis errónea y no debería repetirse");
			         messageResp.setSystem_error_code("serviceUnavailable");
			         messageResp.setSystem_error_description("La solicitud contiene sintaxis errónea y no debería repetirse");
					 break;
	        case 404:    //400 Bad Request  
		  	 	 	 messageResp.setVersion("1");
			         messageResp.setSeverity("ERROR");
			         messageResp.setHttp_status("400");
			         messageResp.setError_code("badRequest");
			         messageResp.setError_message("La solicitud contiene sintaxis errónea y no debería repetirse");
			         messageResp.setSystem_error_code("serviceUnavailable");
			         messageResp.setSystem_error_description("La solicitud contiene sintaxis errónea y no debería repetirse");
					 break;
	        case 405:    //400 Bad Request  
		  	 	 	 messageResp.setVersion("1");
			         messageResp.setSeverity("ERROR");
			         messageResp.setHttp_status("400");
			         messageResp.setError_code("badRequest");
			         messageResp.setError_message("La solicitud contiene sintaxis errónea y no debería repetirse");
			         messageResp.setSystem_error_code("serviceUnavailable");
			         messageResp.setSystem_error_description("La solicitud contiene sintaxis errónea y no debería repetirse");
					 break;
	        case 406:    //400 Bad Request  
		  	 	 	 messageResp.setVersion("1");
			         messageResp.setSeverity("ERROR");
			         messageResp.setHttp_status("400");
			         messageResp.setError_code("badRequest");
			         messageResp.setError_message("La solicitud contiene sintaxis errónea y no debería repetirse");
			         messageResp.setSystem_error_code("serviceUnavailable");
			         messageResp.setSystem_error_description("La solicitud contiene sintaxis errónea y no debería repetirse");
					 break;
	        case 407:    //400 Bad Request  
		  	 	 	 messageResp.setVersion("1");
			         messageResp.setSeverity("ERROR");
			         messageResp.setHttp_status("400");
			         messageResp.setError_code("Proxy Authentication Required");
			         messageResp.setError_message("Solicitud de Proxy Requerida");
			         messageResp.setSystem_error_code("Proxy Authentication Required");
			         messageResp.setSystem_error_description("Solicitud de Proxy Requeridae");
					 break;
	        case 408:    //400 Bad Request  
		  	 	 	 messageResp.setVersion("1");
			         messageResp.setSeverity("ERROR");
			         messageResp.setHttp_status("400");
			         messageResp.setError_code("Request Timeout");
			         messageResp.setError_message("La solicitud contiene sintaxis errónea y no debería repetirse");
			         messageResp.setSystem_error_code("Request Timeout");
			         messageResp.setSystem_error_description("La solicitud contiene sintaxis errónea y no debería repetirse");
					 break;
	        case 409:   //400 Bad Request  
		  	 	 	 messageResp.setVersion("1");
			         messageResp.setSeverity("ERROR");
			         messageResp.setHttp_status("400");
			         messageResp.setError_code("Conflict");
			         messageResp.setError_message("La solicitud contiene sintaxis errónea y no debería repetirse");
			         messageResp.setSystem_error_code("Conflict");
			         messageResp.setSystem_error_description("La solicitud contiene sintaxis errónea y no debería repetirse");
					 break;
	        case 410:   //400 Bad Request  
		  	 	 	 messageResp.setVersion("1");
			         messageResp.setSeverity("ERROR");
			         messageResp.setHttp_status("400");
			         messageResp.setError_code("Gone");
			         messageResp.setError_message("La solicitud contiene sintaxis errónea y no debería repetirse");
			         messageResp.setSystem_error_code("Gone");
			         messageResp.setSystem_error_description("La solicitud contiene sintaxis errónea y no debería repetirse");
					 break;
	        case 411:   //400 Bad Request  
		  	 	 	 messageResp.setVersion("1");
			         messageResp.setSeverity("ERROR");
			         messageResp.setHttp_status("411");
			         messageResp.setError_code("Length Required");
			         messageResp.setError_message("La solicitud contiene sintaxis errónea y no debería repetirse");
			         messageResp.setSystem_error_code("Length Required");
			         messageResp.setSystem_error_description("La solicitud contiene sintaxis errónea y no debería repetirse");
					 break;	                 
	        default: break;
	    }
		return messageResp;
	}
	
	public static int ejecutarSCP(String desde, String hacia) {
		StringBuilder strComando = new StringBuilder();
		strComando.append("/usr/bin/scp ");
		strComando.append(desde).append(" ");
		strComando.append(hacia);
		
		logger.info("Comando a ejecutar:" + strComando.toString());
		
		Process pros;
		try {
			pros = Runtime.getRuntime().exec(strComando.toString());
			pros.waitFor();
			int valorSalida = pros.exitValue();
			if(valorSalida != 0) {
				logger.error("Error scp: " + IOUtils.toString(pros.getErrorStream()));
				valorSalida=-1;
			}
			return valorSalida;
		} catch (Throwable e) {
			logger.error("Error al ejecutar el comando scp", e);
		}
		
		return -1;
	}
		
	
	public static String toString(Object obj) {
		return ToStringBuilder.reflectionToString(obj);
	}
	
	/** Metodo usado para formatear un importe con separador de miles o decimales
	 * @param monto Es el importe a formatear
	 * @param formato Es el formato a usar. Ejm: <code>###,###.##</code>
	 * @return importe formateado. Si hubiera algun error retorna el monto sin formato */
	public static String formatearImporte(String monto, String formato){
		DecimalFormatSymbols simbolo=new DecimalFormatSymbols();
	    simbolo.setDecimalSeparator('.');
	    simbolo.setGroupingSeparator(',');
	    DecimalFormat formateador = new DecimalFormat(formato,simbolo);
		try {
			double d = Double.parseDouble(monto);
		    monto = formateador.format (d);
		} catch (Exception e) {
		}
		return monto;
	}
	
	public static Map<String, String> obtenerUsuarioConsultaLego(){
		Map<String, String> mapSct = new HashMap<String, String>();		
		mapSct.put("usuario", "TEVHTw==");
		mapSct.put("password", "R0lGT0xFX1dT");		
		return mapSct;
	}
	
	public static String manejoCondicionalConsultaSQL(String valorConsulta, String columnaTabla){
		String retorno = "1 = 1";
		try {
			
			if(StringUtils.isBlank(valorConsulta)){
				return retorno;
			}
			retorno = "UPPER("+columnaTabla+") LIKE UPPER('%"+valorConsulta+"%')";
		} catch (Exception e) {
		}
		return retorno;		
	}
	
	public static String manejoCondicionalRangoFechasSQL(String fecIni, String columnaTablaFecIni,String fecFin, String columnaTablaFecFin){
		String retorno = "1 = 1";
		try {
			
			if(StringUtils.isBlank(fecIni) || StringUtils.isBlank(fecFin)){
				return retorno;
			}
			retorno = "STR_TO_DATE("+columnaTablaFecIni+",'%Y-%m-%d') >= "+ " STR_TO_DATE('"+fecIni+"', '%Y-%m-%d')" + " AND ";
			retorno = retorno + "STR_TO_DATE("+columnaTablaFecFin+",'%Y-%m-%d') >= "+ " STR_TO_DATE('"+fecFin+"', '%Y-%m-%d')";
		} catch (Exception e) {
		}
		return retorno;		
	}
	
	public static List<PerfilOpcion> reordenarPadreHijoMenuOpciones(List<PerfilOpcion> menuOpciones)
    {
		List<PerfilOpcion> resultado = new ArrayList<PerfilOpcion>();
		List<PerfilOpcion> copy = menuOpciones;
		List<PerfilOpcion> hijos = new ArrayList<PerfilOpcion>();
		
		for(int a=0;a<menuOpciones.size();a++){
			PerfilOpcion tmpPadre = menuOpciones.get(a);
			if(tmpPadre.getPadre().equalsIgnoreCase("0"))
			{
				for(int e=0;e<copy.size();e++){
					if(copy.get(e).getPadre().equals(String.valueOf(tmpPadre.getIdperfilopcion()))){
						hijos.add(copy.get(e));
					}
				}
				tmpPadre.setHijos(hijos);
				resultado.add(tmpPadre);
				hijos = new ArrayList<PerfilOpcion>();
			}
			
		}
		
		return resultado;
    }
	
	public static String cifrarToken(String claveCifrado,String email)
    {
		String token = "";
		try {
			byte[] llave = claveCifrado.getBytes("UTF-8");
			AESUtil cu=new AESUtil(llave);		
			token=cu.cifrarCadena(email);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return token;
    }
}
