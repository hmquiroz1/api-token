package pe.com.hypersoft.apitoken.service;

import pe.com.hypersoft.apitoken.bean.ObjetoRespuestaParametroAll;
import pe.com.hypersoft.apitoken.bean.ObjetoRespuestaParametroUnit;
import pe.com.hypersoft.apitoken.bean.ObjetoRespuestaUO;
import pe.com.hypersoft.apitoken.bean.ParametroBean;
import pe.com.hypersoft.apitoken.bean.UOSistemaBean;

public interface ParametroService {
	
	ObjetoRespuestaUO agregarUO(UOSistemaBean uoSistemaBean);
	ObjetoRespuestaUO obtenerUO(UOSistemaBean uoSistemaBean);
	ObjetoRespuestaUO actualizarUO(UOSistemaBean uoSistemaBean);
	ObjetoRespuestaUO eliminarUO(UOSistemaBean uoSistemaBean);	
	ObjetoRespuestaParametroAll listarParametroAll(ParametroBean parametroBean);
	ObjetoRespuestaParametroUnit listarParametroUnit(ParametroBean parametroBean);
}
