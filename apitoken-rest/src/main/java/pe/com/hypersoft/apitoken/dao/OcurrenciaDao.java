package pe.com.hypersoft.apitoken.dao;

import java.util.List;

import pe.com.hypersoft.apitoken.bean.OcurrenciaBean;
import pe.com.hypersoft.apitoken.bean.UOSistemaBean;

public interface OcurrenciaDao {

	List<OcurrenciaBean> listarOcurrencias(OcurrenciaBean ocurrenciaBean);
	int agregarOcurrencia(OcurrenciaBean ocurrenciaBean);
	List<OcurrenciaBean> obtenerOcurrencia(OcurrenciaBean ocurrenciaBean);
	int actualizarOcurrencia(OcurrenciaBean ocurrenciaBean);
	int eliminarOcurrencia(OcurrenciaBean ocurrenciaBean);
}
