package pe.com.hypersoft.apitoken.dao;

import java.util.List;

import pe.com.hypersoft.apitoken.bean.AgenciaBean;

public interface AgenciaDao {

	List<AgenciaBean> listarAgencia(AgenciaBean agenciaBean);	
	String registrarAgencia(AgenciaBean agenciaBean);
}
