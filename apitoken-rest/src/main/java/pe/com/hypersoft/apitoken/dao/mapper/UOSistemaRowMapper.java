package pe.com.hypersoft.apitoken.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import pe.com.hypersoft.apitoken.bean.UOSistemaBean;
import pe.com.hypersoft.apitoken.bean.UsuarioRestBean;
import pe.com.hypersoft.apitoken.bean.UsuarioSistemaBean;

@Component
@SuppressWarnings("rawtypes")
public class UOSistemaRowMapper implements RowMapper {

	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		UOSistemaBean uo = new UOSistemaBean();
		uo.setId(rs.getLong("ID_UO"));
		uo.setDescripcion(rs.getString("DESCRIPCION"));
		uo.setNombre(rs.getString("NOMBRE"));
		uo.setFecinivig(rs.getString("FECINIVIG"));
		uo.setFecfinvig(rs.getString("FECFINVIG"));		
		uo.setEstado(rs.getString("ESTADO"));		
		return uo;
	}
}