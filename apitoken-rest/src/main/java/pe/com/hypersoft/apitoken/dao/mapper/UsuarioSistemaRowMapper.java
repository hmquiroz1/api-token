package pe.com.hypersoft.apitoken.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import pe.com.hypersoft.apitoken.bean.UsuarioRestBean;
import pe.com.hypersoft.apitoken.bean.UsuarioSistemaBean;

@Component
@SuppressWarnings("rawtypes")
public class UsuarioSistemaRowMapper implements RowMapper {

	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		UsuarioSistemaBean usuario = new UsuarioSistemaBean();
		usuario.setId(rs.getLong("ID_USUARIO"));
		usuario.setRegistro(rs.getString("REGISTRO"));
		usuario.setEmail(rs.getString("EMAIL"));
		usuario.setNombre(rs.getString("NOMBRE"));
		usuario.setPaterno(rs.getString("PATERNO"));
		usuario.setMaterno(rs.getString("MATERNO"));
		usuario.setTipoDoc(rs.getString("TIPODOC"));
		usuario.setNroDoc(rs.getString("NRODOC"));
		usuario.setEstadoCivil(rs.getString("ESTADOCIVIL"));
		usuario.setFecNacimiento(rs.getString("FECNACIMIENTO"));
		usuario.setEmail(rs.getString("EMAIL"));
		usuario.setTelefono(rs.getString("TELEFONO"));
		usuario.setEstado(rs.getString("ESTADO"));
		usuario.setNomEdificio(rs.getString("NOMEDIFICIO"));
		usuario.setPerfil(rs.getString("PERFIL"));
		usuario.setIdcc(rs.getString("ID_CC"));
		usuario.setIduo(rs.getString("ID_UO"));
		return usuario;
	}
}