package pe.com.hypersoft.apitoken.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import pe.com.hypersoft.apitoken.bean.AgenciaBean;
import pe.com.hypersoft.apitoken.bean.ObjetoRespuestaAgencia;
import pe.com.hypersoft.apitoken.bean.ObjetoRespuestaOcurrencia;
import pe.com.hypersoft.apitoken.bean.ObjetoRespuestaUO;
import pe.com.hypersoft.apitoken.bean.ObjetoRespuestaUsuario;
import pe.com.hypersoft.apitoken.bean.OcurrenciaBean;
import pe.com.hypersoft.apitoken.bean.PerfilOpcion;
import pe.com.hypersoft.apitoken.bean.UOSistemaBean;
import pe.com.hypersoft.apitoken.bean.UsuarioSistemaBean;
import pe.com.hypersoft.apitoken.dao.AgenciaDao;
import pe.com.hypersoft.apitoken.dao.OcurrenciaDao;
import pe.com.hypersoft.apitoken.dao.SeguridadDao;
import pe.com.hypersoft.apitoken.service.AgenciaService;
import pe.com.hypersoft.apitoken.service.OcurrenciaService;
import pe.com.hypersoft.apitoken.service.SeguridadService;
import pe.com.hypersoft.apitoken.util.Constante;

@Service
public class AgenciaServiceImpl implements AgenciaService {
	
	@Autowired
	AgenciaDao agenciaDao;
		
	
	public ObjetoRespuestaAgencia listarAgencia(AgenciaBean agenciaBean) {
		Constante.loggerTokenApi.info("========== INICIO listarAgencias ==========");		
		ObjetoRespuestaAgencia data = new ObjetoRespuestaAgencia();		
		List<AgenciaBean> listaAgencias= new ArrayList<AgenciaBean>();
		
		try {			
			listaAgencias = agenciaDao.listarAgencia(agenciaBean);	
			if(listaAgencias != null){
				if(listaAgencias.size() > 0){				
					Constante.loggerTokenApi.info("Hay: "+listaAgencias.size() + " registros encontrados.");
					data.setListaAgencias(listaAgencias);
					data.setCodigo(Constante.PARAMETRO.COD_RPTA_EXITO);
				} else {
					data.setCodigo(Constante.PARAMETRO.COD_RPTA_NO_EXITO);
				}
			}else{
				data.setCodigo(Constante.PARAMETRO.COD_RPTA_NO_EXITO);
			}
		} catch (Exception e) {
			data.setCodigo(Constante.PARAMETRO.COD_RPTA_EXCEPTION);
			Constante.loggerTokenApi.error(Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR+"al consultar si existe agencias registradas: " + e);
		}
		Constante.loggerTokenApi.info("========== FIN listarAgencias ==========");
		return data;
	}
		
	public String registrarAgencia(AgenciaBean agenciaBean) {
		Constante.loggerTokenApi.info("========== INICIO listarAgencias ==========");		
		ObjetoRespuestaAgencia data = new ObjetoRespuestaAgencia();		
		String listaAgencias= "";
		
		try {			
			listaAgencias = agenciaDao.registrarAgencia(agenciaBean);
			
		} catch (Exception e) {
			data.setCodigo(Constante.PARAMETRO.COD_RPTA_EXCEPTION);
			Constante.loggerTokenApi.error(Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR+"al consultar si existe agencias registradas: " + e);
		}
		Constante.loggerTokenApi.info("========== FIN listarAgencias ==========");
		return listaAgencias;
	}
	
}
