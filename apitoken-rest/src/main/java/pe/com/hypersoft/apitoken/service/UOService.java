package pe.com.hypersoft.apitoken.service;

import pe.com.hypersoft.apitoken.bean.ObjetoRespuestaUO;
import pe.com.hypersoft.apitoken.bean.UOSistemaBean;

public interface UOService {
	
	ObjetoRespuestaUO agregarUO(UOSistemaBean uoSistemaBean);
	ObjetoRespuestaUO obtenerUO(UOSistemaBean uoSistemaBean);
	ObjetoRespuestaUO actualizarUO(UOSistemaBean uoSistemaBean);
	ObjetoRespuestaUO eliminarUO(UOSistemaBean uoSistemaBean);
	ObjetoRespuestaUO listarUO(UOSistemaBean uoSistemaBean);
}
