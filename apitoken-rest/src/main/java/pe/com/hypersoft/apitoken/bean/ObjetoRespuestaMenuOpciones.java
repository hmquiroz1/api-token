package pe.com.hypersoft.apitoken.bean;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
public class ObjetoRespuestaMenuOpciones implements Serializable{
	
	private String codigo;
	private String valor;	
	private List<PerfilOpcion> listaMenuOpciones;	
	
	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}
	
	public String getValor() {
		return valor;
	}

	public List<PerfilOpcion> getListaMenuOpciones() {
		return listaMenuOpciones;
	}

	public void setListaMenuOpciones(List<PerfilOpcion> listaMenuOpciones) {
		this.listaMenuOpciones = listaMenuOpciones;
	}
	
	
}