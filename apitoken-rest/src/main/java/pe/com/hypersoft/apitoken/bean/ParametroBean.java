package pe.com.hypersoft.apitoken.bean;

import java.io.Serializable;
import java.util.Date;


@SuppressWarnings("serial")
public class ParametroBean implements Serializable{

	private Integer id;	
	private String descripcion;
	private Integer valor;	
	private String texto;
	private String tipo;	
	private String estado;	
	
	public ParametroBean(){
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Integer getValor() {
		return valor;
	}

	public void setValor(Integer valor) {
		this.valor = valor;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}
			
		
	
}
