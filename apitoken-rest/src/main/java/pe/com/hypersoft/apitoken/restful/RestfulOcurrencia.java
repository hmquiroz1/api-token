package pe.com.hypersoft.apitoken.restful;

import java.io.Serializable;
import java.util.List;

import pe.com.hypersoft.apitoken.bean.MessageError;
import pe.com.hypersoft.apitoken.bean.ObjetoRespuestaOcurrencia;
import pe.com.hypersoft.apitoken.bean.ObjetoRespuestaUsuario;

@SuppressWarnings("serial")
public class RestfulOcurrencia implements Serializable{
	
	private ObjetoRespuestaOcurrencia response;
	private MessageError messageError;
	
	public ObjetoRespuestaOcurrencia getData() {
		return response;
	}
	public void setData(ObjetoRespuestaOcurrencia data) {
		this.response = data;
	}
	public MessageError getMessageError() {
		return messageError;
	}
	public void setMessageError(MessageError messageError) {
		this.messageError = messageError;
	}
		
}