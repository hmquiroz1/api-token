package pe.com.hypersoft.apitoken.bean;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
public class ObjetoRespuestaParametroAll implements Serializable{
	
	private String codigo;
	private String valor;
	private List<CmbParametroBean> listaAppOrigen;
	private List<CmbParametroBean> listaEstadoInc;
	private List<CmbParametroBean> listaCambioInc;
	private List<CmbParametroBean> listaAsisteInc;
	private List<CmbParametroBean> listaDocuRemedy;
	private List<CmbParametroBean> listaEquipoResp;
	private List<CmbParametroBean> listaImpacto;
	private List<CmbParametroBean> listaUrgencia;
	private List<CmbParametroBean> listaPrioridad;
	private List<CmbParametroBean> listaWarroom;
	private List<CmbParametroBean> listaGrupoRemedy;
	
	public List<CmbParametroBean> getListaEstadoInc() {
		return listaEstadoInc;
	}

	public void setListaEstadoInc(List<CmbParametroBean> listaEstadoInc) {
		this.listaEstadoInc = listaEstadoInc;
	}

	public List<CmbParametroBean> getListaCambioInc() {
		return listaCambioInc;
	}

	public void setListaCambioInc(List<CmbParametroBean> listaCambioInc) {
		this.listaCambioInc = listaCambioInc;
	}

	public List<CmbParametroBean> getListaAsisteInc() {
		return listaAsisteInc;
	}

	public void setListaAsisteInc(List<CmbParametroBean> listaAsisteInc) {
		this.listaAsisteInc = listaAsisteInc;
	}

	public List<CmbParametroBean> getListaDocuRemedy() {
		return listaDocuRemedy;
	}

	public void setListaDocuRemedy(List<CmbParametroBean> listaDocuRemedy) {
		this.listaDocuRemedy = listaDocuRemedy;
	}

	public List<CmbParametroBean> getListaEquipoResp() {
		return listaEquipoResp;
	}

	public void setListaEquipoResp(List<CmbParametroBean> listaEquipoResp) {
		this.listaEquipoResp = listaEquipoResp;
	}

	public List<CmbParametroBean> getListaImpacto() {
		return listaImpacto;
	}

	public void setListaImpacto(List<CmbParametroBean> listaImpacto) {
		this.listaImpacto = listaImpacto;
	}

	public List<CmbParametroBean> getListaUrgencia() {
		return listaUrgencia;
	}

	public void setListaUrgencia(List<CmbParametroBean> listaUrgencia) {
		this.listaUrgencia = listaUrgencia;
	}

	public List<CmbParametroBean> getListaPrioridad() {
		return listaPrioridad;
	}

	public void setListaPrioridad(List<CmbParametroBean> listaPrioridad) {
		this.listaPrioridad = listaPrioridad;
	}

	public List<CmbParametroBean> getListaWarroom() {
		return listaWarroom;
	}

	public void setListaWarroom(List<CmbParametroBean> listaWarroom) {
		this.listaWarroom = listaWarroom;
	}

	public List<CmbParametroBean> getListaGrupoRemedy() {
		return listaGrupoRemedy;
	}

	public void setListaGrupoRemedy(List<CmbParametroBean> listaGrupoRemedy) {
		this.listaGrupoRemedy = listaGrupoRemedy;
	}

	public void setListaAppOrigen(List<CmbParametroBean> listaAppOrigen) {
		this.listaAppOrigen = listaAppOrigen;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}
	
	public String getValor() {
		return valor;
	}

	public List<CmbParametroBean> getListaAppOrigen() {
		return listaAppOrigen;
	}

	public void setListaUO(List<CmbParametroBean> listaAppOrigen) {
		this.listaAppOrigen = listaAppOrigen;
	}
		
}