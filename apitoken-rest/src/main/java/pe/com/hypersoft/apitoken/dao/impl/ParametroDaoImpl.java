package pe.com.hypersoft.apitoken.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import pe.com.hypersoft.apitoken.bean.ParametroBean;
import pe.com.hypersoft.apitoken.bean.PerfilOpcion;
import pe.com.hypersoft.apitoken.bean.UOSistemaBean;
import pe.com.hypersoft.apitoken.bean.UsuarioRestBean;
import pe.com.hypersoft.apitoken.dao.ParametroDao;
import pe.com.hypersoft.apitoken.dao.SeguridadDao;
import pe.com.hypersoft.apitoken.dao.UODao;
import pe.com.hypersoft.apitoken.dao.mapper.ParametroRowMapper;
import pe.com.hypersoft.apitoken.dao.mapper.PerfilOpcionMapper;
import pe.com.hypersoft.apitoken.dao.mapper.UOSistemaRowMapper;
import pe.com.hypersoft.apitoken.dao.mapper.UsuarioRestRowMapper;
import pe.com.hypersoft.apitoken.dao.mapper.UsuarioSistemaRowMapper;
import pe.com.hypersoft.apitoken.util.Funcion;
import pe.com.hypersoft.apitoken.util.Utilitario;

import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import pe.com.hypersoft.apitoken.bean.UsuarioSistemaBean;

@Repository
public class ParametroDaoImpl implements ParametroDao {

	@Autowired
	JdbcTemplate jdbcTemplate;	
	
	@Autowired
	private NamedParameterJdbcTemplate namedJdbcTemplate;
	
	@Autowired
	UOSistemaRowMapper uoSistemaRowMapper;
	
	@Autowired
	ParametroRowMapper parametroRowMapper;
	
	@Autowired
	PerfilOpcionMapper perfilOpcionMapper;
	 
	@Value("${cifrado.token.key}")
	private String claveCifrado;	
		
	
	@SuppressWarnings("unchecked")
	public List<UOSistemaBean> obtenerUO(UOSistemaBean uoSistemaBean) {		
		StringBuilder sql=new StringBuilder();
		sql.append("select ID_UO,DESCRIPCION,NOMBRE,FECINIVIG,FECFINVIG,ESTADO from APP_SEGURIDAD.UO WHERE ID_UO = " + uoSistemaBean.getId());		
						
		return jdbcTemplate.query(sql.toString(), uoSistemaRowMapper);
	}
	
	public int agregarUO(UOSistemaBean uoSistemaBean) {	
		int rows = 0;
		String sql1 = "";
		sql1  = sql1 + "INSERT INTO APP_SEGURIDAD.UO (DESCRIPCION,NOMBRE,FECINIVIG,FECFINVIG,ESTADO) ";
		sql1  = sql1 + "VALUES (:descripcion,:nombre,:fecinivig,:fecfinvig,:estado) "; 
		
		SqlParameterSource namedParameters = new MapSqlParameterSource();		
		
		if(uoSistemaBean.getDescripcion() != null) ((MapSqlParameterSource) namedParameters).addValue("descripcion", uoSistemaBean.getDescripcion());
		if(uoSistemaBean.getNombre() != null) ((MapSqlParameterSource) namedParameters).addValue("nombre", uoSistemaBean.getNombre());
		if(uoSistemaBean.getFecinivig() != null) ((MapSqlParameterSource) namedParameters).addValue("fecinivig", Funcion.stringToDate(uoSistemaBean.getFecinivig(), "dd/MM/yyyy"), Types.DATE);
		if(uoSistemaBean.getFecfinvig() != null) ((MapSqlParameterSource) namedParameters).addValue("fecfinvig", Funcion.stringToDate(uoSistemaBean.getFecfinvig(), "dd/MM/yyyy"), Types.DATE);	
		if(uoSistemaBean.getEstado() != null) ((MapSqlParameterSource) namedParameters).addValue("estado", uoSistemaBean.getEstado());
		
		rows = namedJdbcTemplate.update(sql1, namedParameters);	
				
		return rows;
	}
	
	public int actualizarUO(UOSistemaBean uoSistemaBean) {	
		int rows = 0;
		String sql1 = "";
		sql1  = sql1 + "UPDATE APP_SEGURIDAD.UO SET ";
		
		if(uoSistemaBean.getNombre() != null) sql1 = sql1 + " NOMBRE = :nombre,";
		if(uoSistemaBean.getDescripcion() != null) sql1 = sql1 + " DESCRIPCION = :descripcion,";
		if(uoSistemaBean.getFecinivig() != null) sql1 = sql1 + " FECINIVIG = :fecinivig,";
		if(uoSistemaBean.getFecfinvig() != null) sql1 = sql1 + " FECFINVIG = :fecfinvig,";
		if(uoSistemaBean.getEstado() != null) sql1 = sql1 + " ESTADO = :estado";
		
		sql1 = sql1 + " WHERE ID_UO = :id ";
		
		SqlParameterSource namedParameters = new MapSqlParameterSource();		
		
		if(uoSistemaBean.getNombre() != null) ((MapSqlParameterSource) namedParameters).addValue("nombre", uoSistemaBean.getNombre());
		if(uoSistemaBean.getDescripcion() != null) ((MapSqlParameterSource) namedParameters).addValue("descripcion", uoSistemaBean.getDescripcion());
		if(uoSistemaBean.getFecinivig() != null) ((MapSqlParameterSource) namedParameters).addValue("fecinivig", Funcion.stringToDate(uoSistemaBean.getFecinivig(), "dd/MM/yyyy"), Types.DATE);
		if(uoSistemaBean.getFecfinvig() != null) ((MapSqlParameterSource) namedParameters).addValue("fecfinvig", Funcion.stringToDate(uoSistemaBean.getFecfinvig(), "dd/MM/yyyy"), Types.DATE);
		if(uoSistemaBean.getEstado() != null) ((MapSqlParameterSource) namedParameters).addValue("estado", uoSistemaBean.getEstado());
				
		((MapSqlParameterSource) namedParameters).addValue("id", uoSistemaBean.getId());			
		
		rows = namedJdbcTemplate.update(sql1, namedParameters);	
				
		return rows;
	}
	
	
	@SuppressWarnings("unchecked")
	public int eliminarUO(UOSistemaBean uoSistemaBean) {		
		StringBuilder sql=new StringBuilder();
		sql.append("DELETE FROM APP_SEGURIDAD.UO WHERE ID_UO = " + uoSistemaBean.getId());		
				
		return jdbcTemplate.update(sql.toString());
	}
	
	@SuppressWarnings("unchecked")
	public List<ParametroBean> listarParametroAll(ParametroBean parametroBean) {		
		StringBuilder sql=new StringBuilder();
		sql.append("SELECT TIPO_PARAM,VAL_PARAM,TXT_PARAM FROM APP_SIGRI.PARAMETROS ");		
		sql.append("WHERE ESTADO = '1' ");		
				
		return jdbcTemplate.query(sql.toString(), parametroRowMapper);
	}
	
	@SuppressWarnings("unchecked")
	public List<ParametroBean> listarParametroUnit(ParametroBean parametroBean) {		
		StringBuilder sql=new StringBuilder();
		sql.append("SELECT TIPO_PARAM,VAL_PARAM,TXT_PARAM FROM APP_SIGRI.PARAMETROS ");		
		sql.append("WHERE ESTADO = '1' ");
		sql.append("AND TIPO_PARAM = '" + parametroBean.getTipo() + "' ");
		sql.append("ORDER BY VAL_PARAM ASC ");		
				
		return jdbcTemplate.query(sql.toString(), parametroRowMapper);
	}
}
