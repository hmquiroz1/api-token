package pe.com.hypersoft.apitoken.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import pe.com.hypersoft.apitoken.bean.UsuarioRestBean;

@Component
@SuppressWarnings("rawtypes")
public class UsuarioRestRowMapper implements RowMapper {

	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {

		UsuarioRestBean usuario = new UsuarioRestBean();
		usuario.setId(rs.getLong("ID"));
		
		return usuario;
	}
}