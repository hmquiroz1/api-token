package pe.com.hypersoft.apitoken.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import pe.com.hypersoft.apitoken.bean.ObjetoRespuestaMenuOpciones;
import pe.com.hypersoft.apitoken.bean.ObjetoRespuestaUsuario;
import pe.com.hypersoft.apitoken.bean.PerfilOpcion;
import pe.com.hypersoft.apitoken.bean.UsuarioSistemaBean;
import pe.com.hypersoft.apitoken.dao.SeguridadDao;
import pe.com.hypersoft.apitoken.service.SeguridadService;
import pe.com.hypersoft.apitoken.util.Constante;

@Service
public class SeguridadServiceImpl implements SeguridadService {
	
	@Autowired
	SeguridadDao seguridadDao;
	
	public int autenticarUsuario(String usuario, String password) {
		int resp= 0;
		
		try{
			resp = seguridadDao.autenticarUsuario(usuario, password);
		} catch(Exception e) {
			resp=-1;
			Constante.loggerSeguridad.error(Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR+"al autenticarUsuario ["+usuario+"] : " + e);
		}
		return resp;
	}
	
	public ObjetoRespuestaUsuario consultarSiExisteToken(UsuarioSistemaBean usuarioSistemaBean) {
		Constante.loggerTokenApi.info("========== INICIO consultarSiExisteToken ==========");		
		ObjetoRespuestaUsuario data = new ObjetoRespuestaUsuario();		
		List<UsuarioSistemaBean> listaUsuarios = new ArrayList<UsuarioSistemaBean>();
		
		try {			
			listaUsuarios = seguridadDao.consultarSiExisteToken(usuarioSistemaBean);			
			if(listaUsuarios.size() > 0){				
				Constante.loggerGeneral.info("Hay: "+listaUsuarios.size() + " registros encontrados.");
				data.setListaUsuarios(listaUsuarios);
				data.setCodigo(Constante.PARAMETRO.COD_RPTA_EXITO);
			} else {
				data.setCodigo(Constante.PARAMETRO.COD_RPTA_NO_EXITO);
			}
		} catch (Exception e) {
			data.setCodigo(Constante.PARAMETRO.COD_RPTA_EXCEPTION);
			Constante.loggerTokenApi.error(Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR+"al consultar si existe solicitud registrada: " + e);
		}
		Constante.loggerTokenApi.info("========== FIN consultarSiExisteToken ==========");
		return data;
	}
	
	public ObjetoRespuestaUsuario obtenerUsuario(UsuarioSistemaBean usuarioSistemaBean) {
		Constante.loggerTokenApi.info("========== INICIO obtenerUsuario ==========");		
		ObjetoRespuestaUsuario data = new ObjetoRespuestaUsuario();		
		List<UsuarioSistemaBean> listaUsuarios = new ArrayList<UsuarioSistemaBean>();
		
		try {			
			listaUsuarios = seguridadDao.obtenerUsuario(usuarioSistemaBean);	
			if(listaUsuarios != null){
				if(listaUsuarios.size() > 0){				
					Constante.loggerGeneral.info("Hay: "+listaUsuarios.size() + " registros encontrados.");
					data.setListaUsuarios(listaUsuarios);
					data.setCodigo(Constante.PARAMETRO.COD_RPTA_EXITO);
				} else {
					data.setCodigo(Constante.PARAMETRO.COD_RPTA_NO_EXITO);
				}
			}else{
				data.setCodigo(Constante.PARAMETRO.COD_RPTA_NO_EXITO);
			}
		} catch (Exception e) {
			data.setCodigo(Constante.PARAMETRO.COD_RPTA_EXCEPTION);
			Constante.loggerTokenApi.error(Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR+"al consultar si existe solicitud registrada: " + e);
		}
		Constante.loggerTokenApi.info("========== FIN obtenerUsuario ==========");
		return data;
	}
	
	public ObjetoRespuestaUsuario agregarUsuario(UsuarioSistemaBean usuarioSistemaBean) {
		Constante.loggerTokenApi.info("========== INICIO agregarUsuario ==========");		
		ObjetoRespuestaUsuario data = new ObjetoRespuestaUsuario();		
		int respuesta = 0;
		
		try {			
			respuesta = seguridadDao.agregarUsuario(usuarioSistemaBean);	
			
			if(respuesta == 1){			
				Constante.loggerGeneral.info("Se agregó el Usuario: "+usuarioSistemaBean.getRegistro().toString());				
				data.setCodigo(Constante.PARAMETRO.COD_RPTA_EXITO);
			} else {
				data.setCodigo(Constante.PARAMETRO.COD_RPTA_NO_EXITO);
			}
			
		} catch (Exception e) {
			data.setCodigo(Constante.PARAMETRO.COD_RPTA_EXCEPTION);
			Constante.loggerTokenApi.error(Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR+"al consultar si existe solicitud registrada: " + e);
		}
		Constante.loggerTokenApi.info("========== FIN agregarUsuario ==========");
		return data;
	}
	
	public ObjetoRespuestaUsuario actualizarUsuario(UsuarioSistemaBean usuarioSistemaBean) {
		Constante.loggerTokenApi.info("========== INICIO actualizarUsuario ==========");		
		ObjetoRespuestaUsuario data = new ObjetoRespuestaUsuario();		
		int respuesta = 0;
		
		try {			
			respuesta = seguridadDao.actualizarUsuario(usuarioSistemaBean);	
		
			if(respuesta == 1){				
				Constante.loggerGeneral.info("Se actualizó el Usuario: "+usuarioSistemaBean.getId().toString());				
				data.setCodigo(Constante.PARAMETRO.COD_RPTA_EXITO);
			} else {
				data.setCodigo(Constante.PARAMETRO.COD_RPTA_NO_EXITO);
			}
			
		} catch (Exception e) {
			data.setCodigo(Constante.PARAMETRO.COD_RPTA_EXCEPTION);
			Constante.loggerTokenApi.error(Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR+"al consultar si existe solicitud registrada: " + e);
		}
		Constante.loggerTokenApi.info("========== FIN actualizarUsuario ==========");
		return data;
	}
	
	public ObjetoRespuestaUsuario eliminarUsuario(UsuarioSistemaBean usuarioSistemaBean) {
		Constante.loggerTokenApi.info("========== INICIO eliminarsuario ==========");		
		ObjetoRespuestaUsuario data = new ObjetoRespuestaUsuario();		
		int respuesta = 0;
		
		try {			
			respuesta = seguridadDao.eliminarUsuario(usuarioSistemaBean);	
		
			if(respuesta == 1){				
				Constante.loggerGeneral.info("Se eliminó el Usuario: "+usuarioSistemaBean.getId().toString());				
				data.setCodigo(Constante.PARAMETRO.COD_RPTA_EXITO);
			} else {
				data.setCodigo(Constante.PARAMETRO.COD_RPTA_NO_EXITO);
			}
			
		} catch (Exception e) {
			data.setCodigo(Constante.PARAMETRO.COD_RPTA_EXCEPTION);
			Constante.loggerTokenApi.error(Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR+"al consultar si existe solicitud registrada: " + e);
		}
		Constante.loggerTokenApi.info("========== FIN eliminarsuario ==========");
		return data;
	}
	
	public ObjetoRespuestaMenuOpciones listarMenuOpciones(UsuarioSistemaBean usuarioSistemaBean) {
		Constante.loggerTokenApi.info("========== INICIO listarMenuOpciones ==========");		
		ObjetoRespuestaMenuOpciones data = new ObjetoRespuestaMenuOpciones();		
		List<PerfilOpcion> listaMenuOpciones= new ArrayList<PerfilOpcion>();
		
		try {			
			listaMenuOpciones = seguridadDao.listarMenuOpciones(usuarioSistemaBean);	
			if(listaMenuOpciones != null){
				if(listaMenuOpciones.size() > 0){				
					Constante.loggerGeneral.info("Hay: "+listaMenuOpciones.size() + " registros encontrados.");
					data.setListaMenuOpciones(listaMenuOpciones);
					data.setCodigo(Constante.PARAMETRO.COD_RPTA_EXITO);
				} else {
					data.setCodigo(Constante.PARAMETRO.COD_RPTA_NO_EXITO);
				}
			}else{
				data.setCodigo(Constante.PARAMETRO.COD_RPTA_NO_EXITO);
			}
		} catch (Exception e) {
			data.setCodigo(Constante.PARAMETRO.COD_RPTA_EXCEPTION);
			Constante.loggerTokenApi.error(Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR+"al consultar si existe solicitud registrada: " + e);
		}
		Constante.loggerTokenApi.info("========== FIN listarMenuOpciones ==========");
		return data;
	}
}
