package pe.com.hypersoft.apitoken.bean;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
public class ObjetoRespuestaAgencia implements Serializable{
	
	private String codigo;
	private String valor;
	private List<AgenciaBean> listaAgencias;	
	
	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}
	
	public String getValor() {
		return valor;
	}

	public List<AgenciaBean> getListaAgencias() {
		return listaAgencias;
	}

	public void setListaAgencias(List<AgenciaBean> listaAgencias) {
		this.listaAgencias = listaAgencias;
	}

	
		
}