package pe.com.hypersoft.apitoken.dao;

import java.util.List;

import pe.com.hypersoft.apitoken.bean.PerfilOpcion;
import pe.com.hypersoft.apitoken.bean.UsuarioSistemaBean;

public interface SeguridadDao {

	int autenticarUsuario(String usuario,String password);
	List<UsuarioSistemaBean> consultarSiExisteToken(UsuarioSistemaBean usuarioSistemaBean);
	int agregarUsuario(UsuarioSistemaBean usuarioSistemaBean);
	List<UsuarioSistemaBean> obtenerUsuario(UsuarioSistemaBean usuarioSistemaBean);
	int actualizarUsuario(UsuarioSistemaBean usuarioSistemaBean);
	int eliminarUsuario(UsuarioSistemaBean usuarioSistemaBean);
	List<PerfilOpcion> listarMenuOpciones(UsuarioSistemaBean usuarioSistemaBean);
}
