package pe.com.hypersoft.apitoken.restful;

import java.io.Serializable;
import java.util.List;

import pe.com.hypersoft.apitoken.bean.MessageError;
import pe.com.hypersoft.apitoken.bean.ObjetoRespuestaUO;
import pe.com.hypersoft.apitoken.bean.ObjetoRespuestaUsuario;

@SuppressWarnings("serial")
public class RestfulUO implements Serializable{
	
	private ObjetoRespuestaUO response;
	private MessageError messageError;
	
	public ObjetoRespuestaUO getData() {
		return response;
	}
	public void setData(ObjetoRespuestaUO data) {
		this.response = data;
	}
	public MessageError getMessageError() {
		return messageError;
	}
	public void setMessageError(MessageError messageError) {
		this.messageError = messageError;
	}
		
}