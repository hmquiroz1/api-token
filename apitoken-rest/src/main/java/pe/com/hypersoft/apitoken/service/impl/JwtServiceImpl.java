package pe.com.hypersoft.apitoken.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import static java.time.ZoneOffset.UTC;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.IncorrectClaimException;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MissingClaimException;
import io.jsonwebtoken.SignatureAlgorithm;

import pe.com.hypersoft.apitoken.service.JwtService;
import pe.com.hypersoft.apitoken.util.Constante;

@Service
public class JwtServiceImpl implements JwtService {
	
	static final long EXPIRATIONTIME = 300000; // en milisegundos, equivale a 5 minutos
    
    static final String SECRET = "ThisIsASecret"; //Clave Secreta
     
    static final String TOKEN_PREFIX = "Bearer";
     
    static final String HEADER_STRING = "Authorization";
    
    static final String BEARER = "Bearer ";
    
    static final String ISSUER = "apitoken-rest"; //Creador del Token
    
    static final String SUBJECT = "API Token Acceso Web"; //Asunto del Token
    
	public String generateToken(String username) {
		Constante.loggerTokenApi.info("========== INICIO generateToken ==========");
		String token="";
		Date expiration = Date.from(LocalDateTime.now(UTC).plusHours(2).toInstant(UTC));
		
		token = Jwts.builder()
					.setSubject("API Token Acceso Web")
					.setIssuer(ISSUER)
					.setIssuedAt(new Date(System.currentTimeMillis())) //Tiempo de Creacion
	                .setExpiration(new Date(System.currentTimeMillis() + EXPIRATIONTIME)) //Tiempo de Expiracion
	                .claim("email", username)
	                .signWith(SignatureAlgorithm.HS256, SECRET).compact();
		
		Constante.loggerTokenApi.info("========== FIN generateToken ==========");
		return BEARER + token;
	}
	
	public boolean isBearer(String authorization)
	{
	 return authorization != null && authorization.startsWith(BEARER) && authorization.split("\\.").length == 3;
	}
		
	public static Claims decodeJWT(String authorization) {
	    //This line will throw an exception if it is not a signed JWS (as expected)
		Claims claims = null;
		try {
		    claims = Jwts.parser()
		    		.requireSubject(SUBJECT)
		            .setSigningKey(SECRET)
		            .parseClaimsJws(authorization).getBody();
		} catch(MissingClaimException mce) {
		    // the parsed JWT did not have the sub field
		} catch(IncorrectClaimException ice) {
		    // the parsed JWT had a sub field, but its value was not equal to 'jsmith'
		}
	    return claims;
	}
	
	public String verifyToken(String token) {
		Constante.loggerTokenApi.info("========== INICIO verifyToken ==========");		
		String resp="";
		Constante.loggerTokenApi.info("========== FIN verifyToken ==========");
		return resp;
	}	
}
