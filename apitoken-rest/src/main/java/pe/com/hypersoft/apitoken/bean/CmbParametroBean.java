package pe.com.hypersoft.apitoken.bean;

import java.io.Serializable;
import java.util.Date;


@SuppressWarnings("serial")
public class CmbParametroBean implements Serializable{

	private Integer id;	
	private String name;
	
	public CmbParametroBean(){
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}		
		
	
}
