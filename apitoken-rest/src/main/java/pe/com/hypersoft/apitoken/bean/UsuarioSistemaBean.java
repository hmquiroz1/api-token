package pe.com.hypersoft.apitoken.bean;

import java.io.Serializable;
import java.util.Date;


@SuppressWarnings("serial")
public class UsuarioSistemaBean implements Serializable{

	private Long id;
	private String registro;
	private String email;
	private String clave;
	private String nombre;
	private String paterno;
	private String materno;
	private String tipoDoc;
	private String nroDoc;
	private String fecNacimiento;
	private String estadoCivil;
	private String telefono;
	private String estado;
	private String token;
	private String nomEdificio;
	/**
	 * ID del Perfil
	 */
	private String perfil;
	/**
	 * ID del Centro de Costo
	 */
	private String idcc;
	/**
	 * ID de la UO
	 */
	private String iduo;
	
	
	public UsuarioSistemaBean(){
		
	}
	
	/**
	 * Constructor con Parametros
	 */
	public UsuarioSistemaBean(String email,String clave){
		this.email = email;
		this.clave = clave;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
		
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getPaterno() {
		return paterno;
	}
	public void setPaterno(String paterno) {
		this.paterno = paterno;
	}
	public String getMaterno() {
		return materno;
	}
	public void setMaterno(String materno) {
		this.materno = materno;
	}
	public String getTipoDoc() {
		return tipoDoc;
	}
	public void setTipoDoc(String tipoDoc) {
		this.tipoDoc = tipoDoc;
	}
	public String getNroDoc() {
		return nroDoc;
	}
	public void setNroDoc(String nroDoc) {
		this.nroDoc = nroDoc;
	}
	public String getFecNacimiento() {
		return fecNacimiento;
	}
	public void setFecNacimiento(String fecNacimiento) {
		this.fecNacimiento = fecNacimiento;
	}
	public String getEstadoCivil() {
		return estadoCivil;
	}
	public void setEstadoCivil(String estadoCivil) {
		this.estadoCivil = estadoCivil;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getNomEdificio() {
		return nomEdificio;
	}
	public void setNomEdificio(String nomEdificio) {
		this.nomEdificio = nomEdificio;
	}
	public String getPerfil() {
		return perfil;
	}
	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}
	public String getRegistro() {
		return registro;
	}
	public void setRegistro(String registro) {
		this.registro = registro;
	}
	public String getIdcc() {
		return idcc;
	}
	public void setIdcc(String idcc) {
		this.idcc = idcc;
	}
	public String getIduo() {
		return iduo;
	}
	public void setIduo(String iduo) {
		this.iduo = iduo;
	}

}
