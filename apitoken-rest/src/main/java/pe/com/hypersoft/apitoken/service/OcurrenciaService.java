package pe.com.hypersoft.apitoken.service;

import pe.com.hypersoft.apitoken.bean.ObjetoRespuestaOcurrencia;
import pe.com.hypersoft.apitoken.bean.OcurrenciaBean;

public interface OcurrenciaService {		
	
	ObjetoRespuestaOcurrencia listarOcurrencia(OcurrenciaBean ocurrenciaBean);
	ObjetoRespuestaOcurrencia agregarOcurrencia(OcurrenciaBean ocurrenciaBean);
	ObjetoRespuestaOcurrencia obtenerOcurrencia(OcurrenciaBean ocurrenciaBean);
	ObjetoRespuestaOcurrencia actualizarOcurrencia(OcurrenciaBean ocurrenciaBean);
	ObjetoRespuestaOcurrencia eliminarOcurrencia(OcurrenciaBean ocurrenciaBean);
}
