package pe.com.hypersoft.apitoken.service;

import pe.com.hypersoft.apitoken.bean.ObjetoRespuestaMenuOpciones;
import pe.com.hypersoft.apitoken.bean.ObjetoRespuestaUsuario;
import pe.com.hypersoft.apitoken.bean.UsuarioSistemaBean;

public interface SeguridadService {
	
	int autenticarUsuario(String usuario,String password);
	ObjetoRespuestaUsuario consultarSiExisteToken(UsuarioSistemaBean usuarioSistemaBean);
	ObjetoRespuestaUsuario agregarUsuario(UsuarioSistemaBean usuarioSistemaBean);
	ObjetoRespuestaUsuario obtenerUsuario(UsuarioSistemaBean usuarioSistemaBean);
	ObjetoRespuestaUsuario actualizarUsuario(UsuarioSistemaBean usuarioSistemaBean);
	ObjetoRespuestaUsuario eliminarUsuario(UsuarioSistemaBean usuarioSistemaBean);
	ObjetoRespuestaMenuOpciones listarMenuOpciones(UsuarioSistemaBean usuarioSistemaBean);
}
