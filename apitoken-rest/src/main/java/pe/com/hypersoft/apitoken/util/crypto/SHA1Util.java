package pe.com.hypersoft.apitoken.util.crypto;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
 
public class SHA1Util {
 
    /**
     * @param args
     * @throws NoSuchAlgorithmException 
     */
    public String hash(String input) throws NoSuchAlgorithmException {
        MessageDigest mDigest = MessageDigest.getInstance("SHA1");
        byte[] result = mDigest.digest(input.getBytes());
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < result.length; i++) {
            sb.append(Integer.toString((result[i] & 0xff) + 0x100, 16).substring(1));
        }
         
        return sb.toString();
    }
}