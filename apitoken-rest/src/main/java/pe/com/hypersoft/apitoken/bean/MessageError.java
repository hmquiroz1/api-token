package pe.com.hypersoft.apitoken.bean;

import java.io.Serializable;
import java.util.List;

import org.springframework.http.HttpStatus;

@SuppressWarnings("serial")
public class MessageError implements Serializable{
	
	private String version;
	private String severity;
	private String http_status;
	private String error_code;
	private String error_message;
	private String system_error_code;
	private String system_error_description;
	private HttpStatus httpStatus;
	
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getSeverity() {
		return severity;
	}
	public void setSeverity(String severity) {
		this.severity = severity;
	}
	public String getHttp_status() {
		return http_status;
	}
	public void setHttp_status(String http_status) {
		this.http_status = http_status;
	}
	public String getError_code() {
		return error_code;
	}
	public void setError_code(String error_code) {
		this.error_code = error_code;
	}
	public String getError_message() {
		return error_message;
	}
	public void setError_message(String error_message) {
		this.error_message = error_message;
	}
	public String getSystem_error_code() {
		return system_error_code;
	}
	public void setSystem_error_code(String system_error_code) {
		this.system_error_code = system_error_code;
	}
	public String getSystem_error_description() {
		return system_error_description;
	}
	public void setSystem_error_description(String system_error_description) {
		this.system_error_description = system_error_description;
	}
	public HttpStatus getHttpStatus() {
		return httpStatus;
	}
	public void setHttpStatus(HttpStatus httpStatus) {
		this.httpStatus = httpStatus;
	}
			
}