package pe.com.hypersoft.apitoken.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import pe.com.hypersoft.apitoken.bean.AgenciaBean;
import pe.com.hypersoft.apitoken.bean.OcurrenciaBean;
import pe.com.hypersoft.apitoken.dao.AgenciaDao;
import pe.com.hypersoft.apitoken.dao.OcurrenciaDao;
import pe.com.hypersoft.apitoken.dao.mapper.AgenciaRowMapper;
import pe.com.hypersoft.apitoken.dao.mapper.OcurrenciaRowMapper;
import pe.com.hypersoft.apitoken.util.Funcion;
import pe.com.hypersoft.apitoken.util.Utilitario;

import java.sql.Types;
import java.util.List;

@Repository
public class AgenciaDaoImpl implements AgenciaDao {

	@Autowired
	JdbcTemplate jdbcTemplate;	
	
	@Autowired
	private NamedParameterJdbcTemplate namedJdbcTemplate;
	
	@Autowired
	AgenciaRowMapper agenciaRowMapper;
		 
	@Value("${cifrado.token.key}")
	private String claveCifrado;	
		
	
	@SuppressWarnings("unchecked")
	public List<AgenciaBean> listarAgencia(AgenciaBean agenciaBean) {		
		StringBuilder sql=new StringBuilder();
		sql.append("select AG.DISTRITO,AG.PROVINCIA,AG.DEPARTAMENTO,AG.DIRECCION,AG.LAT,AG.LON ");		
		sql.append("from APP_SIADE.AGENCIAS AG ");		
		sql.append("where UPPER(AG.AGENCIA) LIKE UPPER('%"+agenciaBean.getAgencia()+"%')");
		
		return jdbcTemplate.query(sql.toString(), agenciaRowMapper);
	}
	
	@SuppressWarnings("unchecked")
	public String registrarAgencia(AgenciaBean agenciaBean) {		
		System.out.println(agenciaBean.getDistrito());
		System.out.println(agenciaBean.getProvincia());
		System.out.println(agenciaBean.getDepartamento());
		System.out.println(agenciaBean.getDireccion());
		System.out.println(agenciaBean.getLat());
		return "";
	}
			
}
