package pe.com.hypersoft.apitoken.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import pe.com.hypersoft.apitoken.bean.OcurrenciaBean;
import pe.com.hypersoft.apitoken.bean.UOSistemaBean;
import pe.com.hypersoft.apitoken.bean.UsuarioRestBean;
import pe.com.hypersoft.apitoken.bean.UsuarioSistemaBean;

@Component
@SuppressWarnings("rawtypes")
public class OcurrenciaRowMapper implements RowMapper {

	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		OcurrenciaBean oc = new OcurrenciaBean();
		oc.setIdOcurr(rs.getLong("ID_OCURR"));
		oc.setDesccorta(rs.getString("TITULO"));
		oc.setFecIniSuceso(rs.getString("FEC_SUCESO_INI"));
		oc.setFecFinSuceso(rs.getString("FEC_SUCESO_FIN"));
		oc.setColor(rs.getString("COLOR"));
		oc.setTipoOcurr(rs.getString("TIPO_OCURR"));
				
		return oc;
	}
}