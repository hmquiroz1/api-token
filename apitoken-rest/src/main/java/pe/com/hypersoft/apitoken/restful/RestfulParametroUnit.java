package pe.com.hypersoft.apitoken.restful;

import java.io.Serializable;
import java.util.List;

import pe.com.hypersoft.apitoken.bean.MessageError;
import pe.com.hypersoft.apitoken.bean.ObjetoRespuestaParametroAll;
import pe.com.hypersoft.apitoken.bean.ObjetoRespuestaParametroUnit;
import pe.com.hypersoft.apitoken.bean.ObjetoRespuestaUO;
import pe.com.hypersoft.apitoken.bean.ObjetoRespuestaUsuario;

@SuppressWarnings("serial")
public class RestfulParametroUnit implements Serializable{
	
	private ObjetoRespuestaParametroUnit response;
	private MessageError messageError;
	
	public ObjetoRespuestaParametroUnit getData() {
		return response;
	}
	public void setData(ObjetoRespuestaParametroUnit data) {
		this.response = data;
	}
	public MessageError getMessageError() {
		return messageError;
	}
	public void setMessageError(MessageError messageError) {
		this.messageError = messageError;
	}
		
}