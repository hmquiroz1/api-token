package pe.com.hypersoft.apitoken.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import pe.com.hypersoft.apitoken.bean.OcurrenciaBean;
import pe.com.hypersoft.apitoken.dao.OcurrenciaDao;
import pe.com.hypersoft.apitoken.dao.mapper.OcurrenciaRowMapper;
import pe.com.hypersoft.apitoken.util.Funcion;
import pe.com.hypersoft.apitoken.util.Utilitario;

import java.sql.Types;
import java.util.List;

@Repository
public class OcurrenciaDaoImpl implements OcurrenciaDao {

	@Autowired
	JdbcTemplate jdbcTemplate;	
	
	@Autowired
	private NamedParameterJdbcTemplate namedJdbcTemplate;
	
	@Autowired
	OcurrenciaRowMapper ocurrenciaRowMapper;
		 
	@Value("${cifrado.token.key}")
	private String claveCifrado;	
		
	
	@SuppressWarnings("unchecked")
	public List<OcurrenciaBean> listarOcurrencias(OcurrenciaBean ocurrenciaBean) {		
		StringBuilder sql=new StringBuilder();
		sql.append("select OC.ID_OCURR,TRIM(OC.DESC_CORTA) AS TITULO,STR_TO_DATE(OC.FEC_SUCESO_INI, '%Y-%m-%d') AS FEC_SUCESO_INI,STR_TO_DATE(OC.FEC_SUCESO_FIN, '%Y-%m-%d') AS FEC_SUCESO_FIN,OC.COLOR,OC.TIPO_OCURR ");		
		sql.append("from APP_SIADE.OCURRENCIA OC left join APP_SEGURIDAD.CENTRO_COSTO CC on OC.IDCC = CC.IDCC ");
		sql.append("and OC.ID_UO = CC.ID_UO ");		
		sql.append("and OC.ESTADO = 'A' ");
		sql.append("and OC.IDCC = " + ocurrenciaBean.getIdcc() + " ");
		sql.append("and OC.ID_UO = " +  ocurrenciaBean.getIduo() + " ");
				
		return jdbcTemplate.query(sql.toString(), ocurrenciaRowMapper);
	}
	
	@SuppressWarnings("unchecked")
	public List<OcurrenciaBean> obtenerOcurrencia(OcurrenciaBean ocurrenciaBean) {		
		StringBuilder sql=new StringBuilder();
		sql.append("select ID_OCURR,DESC_CORTA,DESC_LARGA,FEC_SUCESO_INI,FEC_SUCESO_FIN,COLOR,TIPO_OCURR,ESTADO from APP_SIADE.OCURRENCIA ");		
		sql.append(Utilitario.manejoCondicionalConsultaSQL(ocurrenciaBean.getIdOcurr().toString(), "ID_OCURR") + " AND ");
		sql.append(Utilitario.manejoCondicionalConsultaSQL(ocurrenciaBean.getDesccorta(), "DESC_CORTA") + " AND ");
		sql.append(Utilitario.manejoCondicionalRangoFechasSQL(ocurrenciaBean.getFecIniSuceso(), "FEC_SUCESO_INI", ocurrenciaBean.getFecFinSuceso(),"FEC_SUCESO_FIN") + " AND ");
		sql.append(Utilitario.manejoCondicionalConsultaSQL("A", "A.ESTADO"));
				
		return jdbcTemplate.query(sql.toString(), ocurrenciaRowMapper);
	}
	
	public int agregarOcurrencia(OcurrenciaBean ocurrenciaBean) {	
		int rows = 0;
		String sql1 = "";
		sql1  = sql1 + "INSERT INTO APP_SIADE.OCURRENCIA (DESC_CORTA,DESC_LARGA,FEC_SUCESO_INI,FEC_SUCESO_FIN,COLOR,TIPO_OCURR,ESTADO,USER_CREACION,FEC_CREACION,IDCC,ID_UO) ";
		sql1  = sql1 + "VALUES (:desc_corta,:desc_larga,:fecini,:fecfin,:color,:tipoOcurr,:estado,:usuarioCreacion,:fecCreacion,:idcc,:iduo) "; 
		
		SqlParameterSource namedParameters = new MapSqlParameterSource();		
		
		if(ocurrenciaBean.getDesccorta() != null) ((MapSqlParameterSource) namedParameters).addValue("desc_corta", ocurrenciaBean.getDesccorta());
		if(ocurrenciaBean.getDesclarga() != null) ((MapSqlParameterSource) namedParameters).addValue("desc_larga", ocurrenciaBean.getDesclarga());
		if(ocurrenciaBean.getFecIniSuceso() != null) ((MapSqlParameterSource) namedParameters).addValue("fecini", Funcion.stringToDate(ocurrenciaBean.getFecIniSuceso(), "dd/MM/yyyy"), Types.DATE);
		if(ocurrenciaBean.getFecFinSuceso() != null) ((MapSqlParameterSource) namedParameters).addValue("fecfin", Funcion.stringToDate(ocurrenciaBean.getFecFinSuceso(), "dd/MM/yyyy"), Types.DATE);	
		if(ocurrenciaBean.getColor() != null) ((MapSqlParameterSource) namedParameters).addValue("color", ocurrenciaBean.getColor());
		if(ocurrenciaBean.getTipoOcurr() != null) ((MapSqlParameterSource) namedParameters).addValue("tipoOcurr", ocurrenciaBean.getTipoOcurr());
		if(ocurrenciaBean.getEstado() != null) ((MapSqlParameterSource) namedParameters).addValue("estado", ocurrenciaBean.getEstado());
		if(ocurrenciaBean.getUsuarioCreacion() != null) ((MapSqlParameterSource) namedParameters).addValue("usuarioCreacion", ocurrenciaBean.getUsuarioCreacion());
		if(ocurrenciaBean.getFecCreacion() != null) ((MapSqlParameterSource) namedParameters).addValue("fecCreacion", Funcion.stringToDate(ocurrenciaBean.getFecCreacion(), "dd/MM/yyyy"), Types.DATE);
		if(ocurrenciaBean.getIdcc() != null) ((MapSqlParameterSource) namedParameters).addValue("idcc", ocurrenciaBean.getIdcc());
		if(ocurrenciaBean.getIduo() != null) ((MapSqlParameterSource) namedParameters).addValue("iduo", ocurrenciaBean.getIduo());
		
		rows = namedJdbcTemplate.update(sql1, namedParameters);	
				
		return rows;
	}
	
	public int actualizarOcurrencia(OcurrenciaBean ocurrenciaBean) {	
		int rows = 0;
		String sql1 = "";
		sql1  = sql1 + "UPDATE APP_SIADE.OCURRENCIA SET ";
		
		if(ocurrenciaBean.getDesccorta() != null) sql1 = sql1 + " DESC_CORTA = :descCorta,";
		if(ocurrenciaBean.getDesclarga() != null) sql1 = sql1 + " DESC_LARGA = :descLarga,";
		if(ocurrenciaBean.getFecIniSuceso() != null) sql1 = sql1 + " FEC_SUCESO_INI = :fecini,";
		if(ocurrenciaBean.getFecFinSuceso() != null) sql1 = sql1 + " FEC_SUCESO_FIN = :fecfin,";
		if(ocurrenciaBean.getColor() != null) sql1 = sql1 + " COLOR = :color,";
		if(ocurrenciaBean.getTipoOcurr() != null) sql1 = sql1 + " TIPO_OCURR = :tipoOcurr,";
		if(ocurrenciaBean.getEstado() != null) sql1 = sql1 + " ESTADO = :estado,";
		if(ocurrenciaBean.getUsuarioModif() != null) sql1 = sql1 + " USER_MODIF = :usuModif,";
		if(ocurrenciaBean.getFecModif() != null) sql1 = sql1 + " FEC_MODIF = :fecModif";
		
		sql1 = sql1 + " WHERE ID_OCURR = :id ";
		
		SqlParameterSource namedParameters = new MapSqlParameterSource();		
		
		if(ocurrenciaBean.getDesccorta() != null) ((MapSqlParameterSource) namedParameters).addValue("descCorta", ocurrenciaBean.getDesccorta());
		if(ocurrenciaBean.getDesclarga() != null) ((MapSqlParameterSource) namedParameters).addValue("descLarga", ocurrenciaBean.getDesclarga());
		if(ocurrenciaBean.getFecIniSuceso() != null) ((MapSqlParameterSource) namedParameters).addValue("fecini", Funcion.stringToDate(ocurrenciaBean.getFecIniSuceso(), "dd/MM/yyyy"), Types.DATE);
		if(ocurrenciaBean.getFecFinSuceso() != null) ((MapSqlParameterSource) namedParameters).addValue("fecfin", Funcion.stringToDate(ocurrenciaBean.getFecFinSuceso(), "dd/MM/yyyy"), Types.DATE);
		if(ocurrenciaBean.getColor() != null) ((MapSqlParameterSource) namedParameters).addValue("color", ocurrenciaBean.getColor());
		if(ocurrenciaBean.getTipoOcurr() != null) ((MapSqlParameterSource) namedParameters).addValue("tipoOcurr", ocurrenciaBean.getTipoOcurr());
		if(ocurrenciaBean.getEstado() != null) ((MapSqlParameterSource) namedParameters).addValue("estado", ocurrenciaBean.getEstado());
		if(ocurrenciaBean.getUsuarioModif() != null) ((MapSqlParameterSource) namedParameters).addValue("usuModif", ocurrenciaBean.getUsuarioModif());
		if(ocurrenciaBean.getFecModif() != null) ((MapSqlParameterSource) namedParameters).addValue("fecModif", Funcion.stringToDate(ocurrenciaBean.getFecModif(), "dd/MM/yyyy"), Types.DATE);
				
		((MapSqlParameterSource) namedParameters).addValue("id", ocurrenciaBean.getIdOcurr());			
		
		rows = namedJdbcTemplate.update(sql1, namedParameters);	
				
		return rows;
	}
	
	
	public int eliminarOcurrencia(OcurrenciaBean ocurrenciaBean) {		
		StringBuilder sql=new StringBuilder();
		sql.append("DELETE FROM APP_SIADE.OCURRENCIA WHERE ID_OCURR = " + ocurrenciaBean.getIdOcurr());		
				
		return jdbcTemplate.update(sql.toString());
	}
}
