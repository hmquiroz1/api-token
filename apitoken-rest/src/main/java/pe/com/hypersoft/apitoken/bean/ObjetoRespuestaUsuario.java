package pe.com.hypersoft.apitoken.bean;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
public class ObjetoRespuestaUsuario implements Serializable{
	
	private String codigo;
	private String valor;
	private List<UsuarioSistemaBean> listaUsuarios;
	private List<PerfilOpcion> listaMenuOpciones;
	private String token;
	
	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}
	
	public String getValor() {
		return valor;
	}

	public List<UsuarioSistemaBean> getListaUsuarios() {
		return listaUsuarios;
	}

	public void setListaUsuarios(List<UsuarioSistemaBean> listaUsuarios) {
		this.listaUsuarios = listaUsuarios;
	}

	public List<PerfilOpcion> getListaMenuOpciones() {
		return listaMenuOpciones;
	}

	public void setListaMenuOpciones(List<PerfilOpcion> listaMenuOpciones) {
		this.listaMenuOpciones = listaMenuOpciones;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	
}