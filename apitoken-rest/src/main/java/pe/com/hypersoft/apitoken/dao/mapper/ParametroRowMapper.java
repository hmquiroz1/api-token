package pe.com.hypersoft.apitoken.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import pe.com.hypersoft.apitoken.bean.ParametroBean;

@Component
@SuppressWarnings("rawtypes")
public class ParametroRowMapper implements RowMapper {

	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		ParametroBean param = new ParametroBean();
		param.setTipo(rs.getString("TIPO_PARAM"));
		param.setValor(rs.getInt("VAL_PARAM"));
		param.setTexto(rs.getString("TXT_PARAM"));
		return param;
	}
}