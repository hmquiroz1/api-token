package pe.com.hypersoft.apitoken.util;

import java.util.ResourceBundle;

public interface Ruta {

	String VALOR = "valores";
	ResourceBundle rb = ResourceBundle.getBundle(VALOR);
	
	String URL_WS_CONSULTA_RBA           = rb.getString("ruta.ws.consulta.rba");
	String URL_WS_SOAP_ENVIO_FILE_UNICO  = rb.getString("ruta.ws.soap.envio.file.unico");
	String URL_WS_REST_ENVIO_FILE_UNICO  = rb.getString("ruta.ws.rest.envio.file.unico");
	String URL_GIFOLE_REST               = rb.getString("ruta.gifole.rest");
	
	String RUTA_GIFOLE_COMPARTIDA        = rb.getString("ruta.gifole.compartida");
	String RUTA_XCOM_DESTINO           = rb.getString("ruta.gifole.destino");
	
}
