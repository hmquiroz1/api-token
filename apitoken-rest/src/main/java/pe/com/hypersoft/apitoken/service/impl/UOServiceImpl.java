package pe.com.hypersoft.apitoken.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import pe.com.hypersoft.apitoken.bean.ObjetoRespuestaUO;
import pe.com.hypersoft.apitoken.bean.ObjetoRespuestaUsuario;
import pe.com.hypersoft.apitoken.bean.PerfilOpcion;
import pe.com.hypersoft.apitoken.bean.UOSistemaBean;
import pe.com.hypersoft.apitoken.bean.UsuarioSistemaBean;
import pe.com.hypersoft.apitoken.dao.SeguridadDao;
import pe.com.hypersoft.apitoken.dao.UODao;
import pe.com.hypersoft.apitoken.service.SeguridadService;
import pe.com.hypersoft.apitoken.service.UOService;
import pe.com.hypersoft.apitoken.util.Constante;

@Service
public class UOServiceImpl implements UOService {
	
	@Autowired
	UODao uoDao;	
	
	

	public ObjetoRespuestaUO obtenerUO(UOSistemaBean uoSistemaBean) {
		Constante.loggerTokenApi.info("========== INICIO obtenerUOo ==========");		
		ObjetoRespuestaUO data = new ObjetoRespuestaUO();		
		List<UOSistemaBean> listaUOs = new ArrayList<UOSistemaBean>();
		
		try {			
			listaUOs = uoDao.obtenerUO(uoSistemaBean);	
			if(listaUOs != null){
				if(listaUOs.size() > 0){				
					Constante.loggerGeneral.info("Hay: "+listaUOs.size() + " registros encontrados.");
					data.setListaUO(listaUOs);
					data.setCodigo(Constante.PARAMETRO.COD_RPTA_EXITO);
				} else {
					data.setCodigo(Constante.PARAMETRO.COD_RPTA_NO_EXITO);
				}
			}else{
				data.setCodigo(Constante.PARAMETRO.COD_RPTA_NO_EXITO);
			}
		} catch (Exception e) {
			data.setCodigo(Constante.PARAMETRO.COD_RPTA_EXCEPTION);
			Constante.loggerTokenApi.error(Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR+"al consultar si existe solicitud registrada: " + e);
		}
		Constante.loggerTokenApi.info("========== FIN obtenerUO ==========");
		return data;
	}
	
	public ObjetoRespuestaUO agregarUO(UOSistemaBean uoSistemaBean) {
		Constante.loggerTokenApi.info("========== INICIO agregarUO ==========");		
		ObjetoRespuestaUO data = new ObjetoRespuestaUO();		
		int respuesta = 0;
		
		try {			
			respuesta = uoDao.agregarUO(uoSistemaBean);	
			
			if(respuesta == 1){			
				Constante.loggerGeneral.info("Se agregó el Usuario: "+uoSistemaBean.getNombre().toString());				
				data.setCodigo(Constante.PARAMETRO.COD_RPTA_EXITO);
			} else {
				data.setCodigo(Constante.PARAMETRO.COD_RPTA_NO_EXITO);
			}
			
		} catch (Exception e) {
			data.setCodigo(Constante.PARAMETRO.COD_RPTA_EXCEPTION);
			Constante.loggerTokenApi.error(Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR+"al consultar si existe solicitud registrada: " + e);
		}
		Constante.loggerTokenApi.info("========== FIN agregarUO ==========");
		return data;
	}
	
	public ObjetoRespuestaUO actualizarUO(UOSistemaBean uoSistemaBean) {
		Constante.loggerTokenApi.info("========== INICIO actualizarUO ==========");		
		ObjetoRespuestaUO data = new ObjetoRespuestaUO();		
		int respuesta = 0;
		
		try {			
			respuesta = uoDao.actualizarUO(uoSistemaBean);	
		
			if(respuesta == 1){				
				Constante.loggerGeneral.info("Se actualizó el Usuario: "+uoSistemaBean.getId().toString());				
				data.setCodigo(Constante.PARAMETRO.COD_RPTA_EXITO);
			} else {
				data.setCodigo(Constante.PARAMETRO.COD_RPTA_NO_EXITO);
			}
			
		} catch (Exception e) {
			data.setCodigo(Constante.PARAMETRO.COD_RPTA_EXCEPTION);
			Constante.loggerTokenApi.error(Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR+"al consultar si existe solicitud registrada: " + e);
		}
		Constante.loggerTokenApi.info("========== FIN actualizarUOo ==========");
		return data;
	}
	
	public ObjetoRespuestaUO eliminarUO(UOSistemaBean uoSistemaBean) {
		Constante.loggerTokenApi.info("========== INICIO eliminarUO ==========");		
		ObjetoRespuestaUO data = new ObjetoRespuestaUO();		
		int respuesta = 0;
		
		try {			
			respuesta = uoDao.eliminarUO(uoSistemaBean);	
		
			if(respuesta == 1){				
				Constante.loggerGeneral.info("Se eliminó el Usuario: "+uoSistemaBean.getId().toString());				
				data.setCodigo(Constante.PARAMETRO.COD_RPTA_EXITO);
			} else {
				data.setCodigo(Constante.PARAMETRO.COD_RPTA_NO_EXITO);
			}
			
		} catch (Exception e) {
			data.setCodigo(Constante.PARAMETRO.COD_RPTA_EXCEPTION);
			Constante.loggerTokenApi.error(Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR+"al consultar si existe solicitud registrada: " + e);
		}
		Constante.loggerTokenApi.info("========== FIN eliminarsuario ==========");
		return data;
	}
	
	public ObjetoRespuestaUO listarUO(UOSistemaBean uoSistemaBean) {
		Constante.loggerTokenApi.info("========== INICIO listarUO ==========");		
		ObjetoRespuestaUO data = new ObjetoRespuestaUO();		
		List<UOSistemaBean> listaUOs= new ArrayList<UOSistemaBean>();
		
		try {			
			listaUOs = uoDao.listarUO(uoSistemaBean);	
			if(listaUOs != null){
				if(listaUOs.size() > 0){				
					Constante.loggerGeneral.info("Hay: "+listaUOs.size() + " registros encontrados.");
					data.setListaUO(listaUOs);
					data.setCodigo(Constante.PARAMETRO.COD_RPTA_EXITO);
				} else {
					data.setCodigo(Constante.PARAMETRO.COD_RPTA_NO_EXITO);
				}
			}else{
				data.setCodigo(Constante.PARAMETRO.COD_RPTA_NO_EXITO);
			}
		} catch (Exception e) {
			data.setCodigo(Constante.PARAMETRO.COD_RPTA_EXCEPTION);
			Constante.loggerTokenApi.error(Constante.MENSAJE.MSJ_HA_OCURRIDO_ERROR+"al consultar si existe solicitud registrada: " + e);
		}
		Constante.loggerTokenApi.info("========== FIN listarUO ==========");
		return data;
	}
}
